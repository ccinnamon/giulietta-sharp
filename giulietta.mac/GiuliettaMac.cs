﻿//
//  GiuliettaMac.cs
//
//  Author:
//       carddamom <carddamom@outlook.pt>
//
//  Copyright (c) 2017 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Eto;

namespace giulietta.mac {

  /// <summary>
  /// Giulietta mac.
  /// </summary>
  public class GiuliettaMac : Giulietta {

    /// <summary>
    /// The entry point of the program, where the program control starts and ends.
    /// </summary>
    [STAThread]
    public static new void Main( ) {
      var editor = new GiuliettaMac( );
      editor.Run( );
    }

    /// <summary>
    /// Gets the platform.
    /// </summary>
    /// <returns>The platform.</returns>
    protected override Platform getPlatform( ) {
      return Platform.Get( Platforms.XamMac2 );
    }
  }
}
