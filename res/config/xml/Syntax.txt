This file explains the syntax colouring part of Saskatoon.

There are two phases to the syntax code the initial load and the colouring of files

Initial Load:

The syntax files are stored internally to Saskatoon, and are loaded by SASSyntaxRoot initWithArray:  for each file SASSyntaxRoot
creates a SASSyntaxFile object and passes th filename to initWithFile.

SASSyntaxFile initWithFile: uses NSXMLParser to parse each XML file creating sets of SASSyntaxLists and SASSyntaxContexts.

SASSyntaxContexts are the <context/> tags found in the kate syntax files - each one has a set of matching rules (represented by
SASSyntaxMatch objects) which are used to define the colour of each section of the file and to switch selections.

SASSyntaxLists are lists of keywords used by the keyword matching object.

Colouring of Files:

Files are coloured by calling the SASSyntaxFile colourString: method.  This method creates a const char* array of the file
(this is done for efficiency purposes), sets the initial syntax colour to dsNormal, finds the initial syntax context and calls
SASSyntaxContext match:.

SASSYntaxMatch match: repeatedly scans the char buffer calling the SASSyntaxMatch match: to find the 'best' match.  Then
the attributes of the context rule are used to change the syntax colour and optionally to change the context.

The SASSyntaxMatch match: routine passes control to one of the matching routines:

-(long)keyword:(const char*)buffer offset:(long)off file:(SASSyntaxFile*)file;
-(long)anyChar:(const char*)buffer offset:(long)off file:(SASSyntaxFile*)file;
-(long)detectChar:(const char*)buffer offset:(long)off file:(SASSyntaxFile*)file;
-(long)detect2Chars:(const char*)buffer offset:(long)off file:(SASSyntaxFile*)file;
-(long)Float:(const char*)buffer offset:(long)off file:(SASSyntaxFile*)file;
-(long)stringDetect:(const char*)buffer offset:(long)off file:(SASSyntaxFile*)file;
-(long)regExpr:(const char*)buffer offset:(long)off file:(SASSyntaxFile*)file;
-(long)Int:(const char*)buffer offset:(long)off file:(SASSyntaxFile*)file;
-(long)hlCOct:(const char*)buffer offset:(long)off file:(SASSyntaxFile*)file;
-(long)hlCHex:(const char*)buffer offset:(long)off file:(SASSyntaxFile*)file;
-(long)rangeDetect:(const char*)buffer offset:(long)off file:(SASSyntaxFile*)file;
-(long)lineContinue:(const char*)buffer offset:(long)off file:(SASSyntaxFile*)file;
-(long)hlCStringChar:(const char*)buffer offset:(long)off file:(SASSyntaxFile*)file;
-(long)hlCChar:(const char*)buffer offset:(long)off file:(SASSyntaxFile*)file;
-(long)includeRules:(const char*)buffer offset:(long)off file:(SASSyntaxFile*)file;

try to match the current text based on their parameters and either return the next match position or NSNotFound.

According to Shark, most of the syntax colouring time is spent in regExpr.


Keith