﻿//
//  NewDialog.cs
//
//  Author:
//       carddamom <carddamom@outlook.pt>
//
//  Copyright (c) 2017 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using Eto.Forms;
using cc.carddamom.giulietta.view.controls.others;
using cc.carddamom.giulietta.view.controls.mine;
using Eto.Drawing;
using System;

namespace cc.carddamom.giulietta.view.dialogs {

  /// <summary>
  /// The dialog for creating a new workspace, project or file
  /// </summary>
  public class NewDialog : Dialog {

    /// <summary>
    /// The width of the dialog.
    /// </summary>
    public static int DIALOG_WIDTH = 700;

    /// <summary>
    /// The height of the dialog.
    /// </summary>
    public static int DIALOG_HEIGHT = 600;

    /// <summary>
    /// Initializes a new instance of the <see cref="T:cc.carddamom.giulietta.view.NewDialog"/> class.
    /// </summary>
    public NewDialog( ) {
      ClientSize = new Size( DIALOG_WIDTH, DIALOG_HEIGHT );
      var layout = new TableLayout( );
      var accordion = new CategoryViewer( new Size( Convert.ToInt32( DIALOG_WIDTH * .4 ), DIALOG_HEIGHT ) );
      var iconGrid = new IconGridLayout( );
      iconGrid.ClientSize = new Size( Convert.ToInt32( DIALOG_WIDTH * .6 ), DIALOG_HEIGHT );
      layout.Rows.Add( new TableRow( new TableCell[ ] { accordion, iconGrid } ) );
      Content = layout;
    }
  }
}
