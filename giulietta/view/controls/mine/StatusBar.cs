﻿//
//  StatusBar.cs
//
//  Author:
//       carddamom <carddamom@outlook.pt>
//
//  Copyright (c) 2017 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using Eto.Forms;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using LanguageExt;
using System.Linq;
using System.Text;
using System;

namespace cc.carddamom.giulietta.view.controls.mine {

  /// <summary>
  /// This represents an StatusBar that is placed at the bottom of a Window, and can contain text aligned to the 
  /// left, center and right
  /// </summary>
  public class StatusBar : Panel {

    private readonly Label leftLabel = new Label( );

    private readonly Label centerLabel = new Label( );

    private readonly Label rightLabel = new Label( );

    /// <summary>
    /// The individual panels that the StatusBar has.
    /// </summary>
    public ICollection<StatusBarPanel> Panels { get; set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="T:cc.carddamom.giulietta.view.StatusBar"/> class.
    /// </summary>
    /// <param name="size">Thw width of the parent container.</param>
    /// <param name="heigth">The height of the parent.</param>
    public StatusBar( int size, int heigth ) {
      var panels = new ObservableCollection<StatusBarPanel>( );
      panels.CollectionChanged += panelChanged;
      Panels = panels;

      Height = Convert.ToInt32( heigth * 0.1 );

      leftLabel.Width = Convert.ToInt32( size * .2 );
      centerLabel.Width = Convert.ToInt32( size * .2 );
      rightLabel.Width = Convert.ToInt32( size * .2 );

      new TableLayout( ).With( ( TableLayout layout ) => {
        layout.Rows.Add( new TableRow( new TableCell[ ] { leftLabel, centerLabel, rightLabel } ) );
        Content = layout;
      } );
    }

    private void panelChanged( object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e ) {

      var sb = new StringBuilder[ ] { new StringBuilder( ), new StringBuilder( ), new StringBuilder( ) };

      Panels.GroupBy( ( StatusBarPanel arg ) => arg.Position ).ForAll( ( arg ) => {
        switch( arg.Key ) {
          case StatusBarPanel.POSITION.LEFT:
            sb[ 0 ].Append( string.Join( "  ", arg.ToList( ) ) );
            break;
          case StatusBarPanel.POSITION.CENTER:
            sb[ 1 ].Append( string.Join( "  ", arg.ToList( ) ) );
            break;
          case StatusBarPanel.POSITION.RIGHT:
            sb[ 2 ].Append( string.Join( "  ", arg.ToList( ) ) );
            break;
        }
        return true;
      } );

      leftLabel.Text = sb[ 0 ].ToString( );
      centerLabel.Text = sb[ 1 ].ToString( );
      rightLabel.Text = sb[ 2 ].ToString( );

    }

  }
}
