﻿//
//  CategoryPanel.cs
//
//  Author:
//       carddamom <carddamom@outlook.pt>
//
//  Copyright (c) 2017 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using Eto.Forms;
using System.Linq;

namespace cc.carddamom.giulietta.view.controls.mine {

  /// <summary>
  /// This class represents a category panel.
  /// </summary>
  public class CategoryPanel : Panel, IEquatable<CategoryPanel> {

    private readonly Label title;

    private List<CategoryItem> panels;

    private readonly Panel bodyPanel;

    /// <summary>
    /// Gets the items in this category
    /// </summary>
    public List<CategoryItem> Items {
      get {
        return panels.AsReadOnly( ).ToList( );
      }
    }

    /// <summary>
    /// Gets or sets the name of the category.
    /// </summary>
    /// <value>The name of the category.</value>
    public string CategoryName {
      get {
        return title.Text;
      }
      set {
        title.Text = value;
      }
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="T:cc.carddamom.giulietta.view.CategoryPanel"/> class.
    /// </summary>
    public CategoryPanel( ) {
      var layout = new TableLayout( );

      title = new Label( );
      title.Text = "";
      title.Height = 30;


      bodyPanel = new Panel( );
      bodyPanel.Content = new TableLayout( );

      layout.Rows.Add( new TableRow( new TableCell[ ] { title } ) );
      layout.Rows.Add( new TableRow( new TableCell[ ] { bodyPanel } ) );

      Content = layout;
    }

    /// <summary>
    /// Determines whether the specified <see cref="cc.carddamom.giulietta.view.CategoryPanel"/> is equal to the current <see cref="T:cc.carddamom.giulietta.view.CategoryPanel"/>.
    /// </summary>
    /// <param name="other">The <see cref="cc.carddamom.giulietta.view.CategoryPanel"/> to compare with the current <see cref="T:cc.carddamom.giulietta.view.CategoryPanel"/>.</param>
    /// <returns><c>true</c> if the specified <see cref="cc.carddamom.giulietta.view.CategoryPanel"/> is equal to the current
    /// <see cref="T:cc.carddamom.giulietta.view.CategoryPanel"/>; otherwise, <c>false</c>.</returns>
    public bool Equals( CategoryPanel other ) {
      return CategoryName.Equals( other.CategoryName );
    }

    /// <summary>
    /// Sets the parent category viewer of this panel
    /// </summary>
    public void setParent( CategoryViewer parent ) {
      title.Width = parent.Width;
      bodyPanel.Width = parent.Width;
    }

    /// <summary>
    /// Adds the given item, if it does not exist.
    /// </summary>
    /// <param name="item">The item to add</param>
    public void addItem( CategoryItem item ) {
      item.setParent( this );
      if( !panels.Contains( item ) ) {
        panels.Add( item );
        var layout = bodyPanel.Content as TableLayout;
        layout.Rows.Add( item );
      }
    }

    /// <summary>
    /// Removes the given item from the panel.
    /// </summary>
    /// <param name="item">The item to remove.</param>
    public void removeItem( CategoryItem item ) {
      panels.Remove( panels.FirstOrDefault( ( arg ) => { return arg.Equals( item ); } ) );
      var layout = bodyPanel.Content as TableLayout;
      layout.Rows.Remove( layout.Rows.FirstOrDefault( ( arg ) => { return item.Equals( arg.Cells[ 0 ].Control as CategoryItem ); } ) );
    }

    /// <summary>
    /// Gets the item count.
    /// </summary>
    public long countItems( ) {
      return panels.LongCount( );
    }

  }
}
