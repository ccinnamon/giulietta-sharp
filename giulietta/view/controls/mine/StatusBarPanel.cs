﻿//
//  StatusBarPanel.cs
//
//  Author:
//       carddamom <carddamom@outlook.pt>
//
//  Copyright (c) 2017 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace cc.carddamom.giulietta.view.controls.mine {

  public class StatusBarPanel {

    public string Text { get; set; }

    public enum POSITION { LEFT, CENTER, RIGHT }

    public POSITION Position { get; set; }

    public StatusBarPanel( POSITION posiion, string text ) {

      Position = posiion;
      Text = text;

    }

    public override string ToString( ) {
      return Text;
    }

  }
}
