﻿//
//  CategoryItem.cs
//
//  Author:
//       carddamom <carddamom@outlook.pt>
//
//  Copyright (c) 2017 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using Eto.Forms;

namespace cc.carddamom.giulietta.view.controls.mine {

  /// <summary>
  /// A category item.
  /// </summary>
  public class CategoryItem : Panel, IEquatable<CategoryItem> {

    private readonly Button categoryButton;

    /// <summary>
    /// Gets or sets the item label.
    /// </summary>
    /// <value>The item label.</value>
    public String ItemLabel {
      get { return categoryButton.Text; }
      set { categoryButton.Text = value ?? categoryButton.Text; }
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="T:cc.carddamom.giulietta.view.CategoryItem"/> class.
    /// </summary>
    public CategoryItem( ) {
      Height = 20;

      categoryButton = new Button( );

      Content = categoryButton;
    }

    /// <summary>
    /// Sets the parent category panel of this item
    /// </summary>
    public void setParent( CategoryPanel parent ) {
      Width = parent.Width;
    }

    /// <summary>
    /// Determines whether the specified <see cref="cc.carddamom.giulietta.view.CategoryItem"/> is equal to the current <see cref="T:cc.carddamom.giulietta.view.CategoryItem"/>.
    /// </summary>
    /// <param name="other">The <see cref="cc.carddamom.giulietta.view.CategoryItem"/> to compare with the current <see cref="T:cc.carddamom.giulietta.view.CategoryItem"/>.</param>
    /// <returns><c>true</c> if the specified <see cref="cc.carddamom.giulietta.view.CategoryItem"/> is equal to the current
    /// <see cref="T:cc.carddamom.giulietta.view.CategoryItem"/>; otherwise, <c>false</c>.</returns>
    public bool Equals( CategoryItem other ) {
      return ItemLabel.Equals( other.ItemLabel );
    }
  }
}
