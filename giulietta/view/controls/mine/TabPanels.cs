﻿//
//  TabPanels.cs
//
//  Author:
//       carddamom <carddamom@outlook.pt>
//
//  Copyright (c) 2017 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using Eto.Forms;
using Eto.Drawing;

namespace cc.carddamom.giulietta.view.controls.mine {

  public enum BAR_POSITION { LEFT, CENTER, BOTTOM };

  public class TabPanels {

    public TabControl Sidebar { get; set; }

    public TabControl Content { get; set; }

    public TabControl BottomBar { get; set; }

    public TabPanels( int width, int height ) {
      Sidebar = new TabControl( );
      Content = new TabControl( );
      BottomBar = new TabControl( );

      BottomBar.Size = new Size( width, Convert.ToInt32( height * .3 ) );
      Sidebar.Size = new Size( Convert.ToInt32( width * .3 ), Convert.ToInt32( height * .64 ) );
      Content.Size = new Size( Convert.ToInt32( width * .7 ), Convert.ToInt32( height * .64 ) );
    }

    public void addToBar( string name, Panel panel, BAR_POSITION position ) {
      var scrollable = new Scrollable( );
      scrollable.Content = panel;

      var tabPage = new TabPage( ).With( ( TabPage page ) => {
        page.Text = name;
        page.Content = panel;
      } );

      switch( position ) {
        case BAR_POSITION.LEFT:
          if( !Sidebar.Pages.Contains( tabPage, new TabPageComparer( ) ) ) {
            Sidebar.Pages.Add( tabPage );
          }
          break;
        case BAR_POSITION.CENTER:
          if( !Content.Pages.Contains( tabPage, new TabPageComparer( ) ) ) {
            Content.Pages.Add( tabPage );
          }
          break;
        case BAR_POSITION.BOTTOM:
          if( !BottomBar.Pages.Contains( tabPage, new TabPageComparer( ) ) ) {
            BottomBar.Pages.Add( tabPage );
          }
          break;
      }
    }

    public void removeFromBar( string name, BAR_POSITION position ) {
      ICollection<TabPage> tabPages = null;
      switch( position ) {
        case BAR_POSITION.LEFT:
          tabPages = Sidebar.Pages;
          break;
        case BAR_POSITION.CENTER:
          tabPages = Content.Pages;
          break;
        case BAR_POSITION.BOTTOM:
          tabPages = BottomBar.Pages;
          break;
      }
      tabPages.Remove( tabPages.FirstOrDefault( ( TabPage arg ) => name.Equals( arg.Text ) ) );
    }

    public ICollection<TabPage> getBar( BAR_POSITION position ) {
      switch( position ) {
        case BAR_POSITION.LEFT:
          return Sidebar.Pages.ToList( ).AsReadOnly( );
        case BAR_POSITION.CENTER:
          return Content.Pages.ToList( ).AsReadOnly( );
        case BAR_POSITION.BOTTOM:
          return BottomBar.Pages.ToList( ).AsReadOnly( );
      }
      throw new NotSupportedException( );
    }

  }

  class TabPageComparer : IEqualityComparer<TabPage> {
    public bool Equals( TabPage x, TabPage y ) {
      return ( x.Text ?? "" ).Equals( y.Text );
    }

    public int GetHashCode( TabPage obj ) {
      return obj.Text.GetHashCode( );
    }
  }

}
