﻿//
//  CategoryViewer.cs
//
//  Author:
//       carddamom <carddamom@outlook.pt>
//
//  Copyright (c) 2017 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System.Collections.Generic;
using Eto.Forms;
using System.Linq;
using Eto.Drawing;

namespace cc.carddamom.giulietta.view.controls.mine {

  /// <summary>
  /// Category viewer.
  /// </summary>
  public class CategoryViewer : Scrollable {

    private readonly List<CategoryPanel> panels;

    private readonly TableLayout accordion;

    /// <summary>
    /// Gets the panels.
    /// </summary>
    public List<CategoryPanel> Panels {
      get {
        return panels;
      }
      private set {
        panels.Clear( );
        panels.AddRange( value );
        accordion.Rows.Clear( );
        panels.ForEach( ( panel ) => { addCategoryPanel( panel ); } );
      }
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="T:cc.carddamom.giulietta.view.CategoryViewer"/> class.
    /// </summary>
    /// <param name="size">The relative size of the viewer.</param>
    public CategoryViewer( Size size ) {
      accordion = new TableLayout( );
      panels = new List<CategoryPanel>( );
      Content = accordion;
      Width = size.Width;
    }

    /// <summary>
    /// Adds the category panel.
    /// </summary>
    public void addCategoryPanel( CategoryPanel panel ) {
      panel.setParent( this );
      if( !panels.Contains( panel ) ) {
        panels.Add( panel );
        accordion.Rows.Add( new TableRow( new TableCell[ ] { panel } ) );
      }
    }

    /// <summary>
    /// Removes the category panel.
    /// </summary>
    public void removeCategoryPanel( CategoryPanel panel ) {
      panels.Remove( panels.FirstOrDefault( ( arg ) => { return arg.Equals( panel ); } ) );
      accordion.Rows.Remove( accordion.Rows.FirstOrDefault( ( TableRow arg ) => {
        return panel.Equals( arg.Cells[ 0 ].Control as CategoryPanel );
      } ) );
    }

    /// <summary>
    /// Gets the size of the category panel.
    /// </summary>
    public long categoryPanelCount( ) {
      return accordion.Rows.LongCount( );
    }
  }
}
