﻿//
//  MainForm.cs
//
//  Author:
//       carddamom <carddamom@outlook.pt>
//
//  Copyright (c) 2016 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using Eto.Forms;
using Eto.Drawing;
using cc.carddamom.giulietta.presenter;
using cc.carddamom.giulietta.view.controls.mine;
using cc.carddamom.giulietta.view.dialogs;

namespace cc.carddamom.giulietta.view {

  /// <summary>
  /// Main form.
  /// </summary>
  public class MainForm : Form {

    private ButtonMenuItem fileMenu;

    private ButtonMenuItem editMenu;

    private ButtonMenuItem windowMenu;

    private ButtonMenuItem helpMenu;

    private StatusBar statusBar;

    private TabPanels tabPanels = new TabPanels( FORM_WIDTH, FORM_HEIGHT );

    private static readonly int FORM_HEIGHT = 768;

    private static readonly int FORM_WIDTH = 1024;

    /// <summary>
    /// Initializes a new instance of the <see cref="T:view.MainForm"/> class.
    /// </summary>
    public MainForm( NewDialog dialog, OpenFileDialog openDialog, SaveFileDialog saveDialog ) {

      Title = "Giulietta\u266F";
      ClientSize = new Size( FORM_WIDTH, FORM_HEIGHT );

      createMenu( dialog, openDialog, saveDialog );
      createToolbar( dialog, openDialog );
      createStatusBar( );

      var layout = new TableLayout( );

      var verticalSplitter = new Splitter( );
      verticalSplitter.Orientation = Orientation.Vertical;

      var horizontalSplitter = new Splitter( );
      horizontalSplitter.Orientation = Orientation.Horizontal;

      layout.Rows.Add( new TableRow( new TableCell( verticalSplitter, true ) ) );
      layout.Rows.Add( new TableRow( new TableCell( statusBar, true ) ) );

      verticalSplitter.Panel2 = tabPanels.BottomBar;
      verticalSplitter.Panel1 = horizontalSplitter;

      horizontalSplitter.Panel1 = tabPanels.Sidebar;
      horizontalSplitter.Panel2 = tabPanels.Content;

      Content = layout;

      statusBar.Panels.Add( new StatusBarPanel( StatusBarPanel.POSITION.LEFT, "Texto" ) );
      statusBar.Panels.Add( new StatusBarPanel( StatusBarPanel.POSITION.CENTER, "Texto" ) );
      statusBar.Panels.Add( new StatusBarPanel( StatusBarPanel.POSITION.RIGHT, "Texto" ) );
    }

    private void createStatusBar( ) {

      //StatusBar Layout
      //
      // Left: Git Branch Information, Current Line and Column
      // Center: ???
      // Right: Keyboard Status
      //
      statusBar = new StatusBar( FORM_WIDTH, FORM_HEIGHT );
    }

    private void createToolbar( NewDialog dialog, OpenFileDialog openDialog ) {
      ToolBar = new ToolBar( );
      ToolBar.Items.Add( new NewCommand( dialog ) );
      ToolBar.Items.Add( new OpenCommand( openDialog ) );
      ToolBar.Items.Add( new SaveCommand( ) );
      ToolBar.Items.Add( new SaveAllCommand( ) );
      ToolBar.Items.AddSeparator( );
      ToolBar.Items.Add( new PreferencesCommand( ) );
      ToolBar.Items.Add( new PageSetupCommand( ) );
      ToolBar.Items.Add( new PrintCommand( ) );
      ToolBar.Items.AddSeparator( );
      ToolBar.Items.Add( new UndoCommand( ) );
      ToolBar.Items.Add( new RedoCommand( ) );
      ToolBar.Items.AddSeparator( );
      ToolBar.Items.Add( new CutCommand( ) );
      ToolBar.Items.Add( new CopyCommand( ) );
      ToolBar.Items.Add( new PasteCommand( ) );
      ToolBar.Items.Add( new DeleteCommand( ) );
      ToolBar.Items.AddSeparator( );
      ToolBar.Items.Add( new FindCommand( ) );
      ToolBar.Items.Add( new FindReplaceCommand( ) );
      ToolBar.Items.AddSeparator( );
      ToolBar.Items.Add( new HelpCommand( ) );
      ToolBar.Items.Add( new VersionCommand( ) );
    }

    private void createMenu( NewDialog dialog, OpenFileDialog openDialog, SaveFileDialog saveDialog ) {
      Menu = new MenuBar( );
      Menu.QuitItem = new QuitCommand( );
      Menu.AboutItem = new VersionCommand( );
      Menu.HelpMenu.Command = new HelpCommand( );

      Menu.Items.Add( createFileMenu( dialog, openDialog, saveDialog ) );
      Menu.Items.Add( createEditMenu( ) );
      Menu.Items.Add( createWindowMenu( ) );
      Menu.Items.Add( createHelpMenu( ) );

      Menu.IncludeSystemItems = MenuBarSystemItems.None;
    }

    private ButtonMenuItem createFileMenu( NewDialog dialog, OpenFileDialog openDialog, SaveFileDialog saveDialog ) {
      fileMenu = new ButtonMenuItem( );
      fileMenu.Text = "&File";
      fileMenu.Items.Add( new NewCommand( dialog ) );
      fileMenu.Items.Add( new OpenCommand( openDialog ) );
      fileMenu.Items.Add( new RecentCommand( ) );
      fileMenu.Items.AddSeparator( );
      fileMenu.Items.Add( new CloseCommand( ) );
      fileMenu.Items.Add( new SaveCommand( ) );
      fileMenu.Items.Add( new SaveAsCommand( saveDialog ) );
      fileMenu.Items.Add( new SaveAllCommand( ) );
      fileMenu.Items.AddSeparator( );
      fileMenu.Items.Add( new RecentCommand( ) );
      fileMenu.Items.AddSeparator( );
      fileMenu.Items.Add( new RevertCommand( ) );
      fileMenu.Items.AddSeparator( );
      fileMenu.Items.Add( new PageSetupCommand( ) );
      fileMenu.Items.Add( new PrintCommand( ) );
      return fileMenu;
    }

    private ButtonMenuItem createEditMenu( ) {
      editMenu = new ButtonMenuItem( );
      editMenu.Text = "&Edit";
      editMenu.Items.Add( new UndoCommand( ) );
      editMenu.Items.Add( new RedoCommand( ) );
      editMenu.Items.AddSeparator( );
      editMenu.Items.Add( new CutCommand( ) );
      editMenu.Items.Add( new CopyCommand( ) );
      editMenu.Items.Add( new PasteCommand( ) );
      editMenu.Items.Add( new DeleteCommand( ) );
      editMenu.Items.AddSeparator( );
      editMenu.Items.Add( new SelectAllCommand( ) );
      editMenu.Items.AddSeparator( );
      editMenu.Items.Add( new FindCommand( ) );
      editMenu.Items.Add( new FindReplaceCommand( ) );
      editMenu.Items.Add( new FindNextCommand( ) );
      editMenu.Items.Add( new FindPreviousCommand( ) );
      editMenu.Items.AddSeparator( );
      editMenu.Items.Add( new PreferencesCommand( ) );
      return editMenu;
    }

    private ButtonMenuItem createWindowMenu( ) {
      windowMenu = new ButtonMenuItem( );
      windowMenu.Text = "&Window";
      windowMenu.Items.Add( new MinimizeCommand( ) );
      windowMenu.Items.Add( new ZoomCommand( ) );
      windowMenu.Items.AddSeparator( );
      windowMenu.Items.Add( new WindowListCommand( ) );
      windowMenu.Items.AddSeparator( );
      windowMenu.Items.Add( new BringAllFrontCommand( ) );
      return windowMenu;
    }

    private ButtonMenuItem createHelpMenu( ) {
      helpMenu = new ButtonMenuItem( );
      helpMenu.Text = "&Help";
      helpMenu.Items.Add( new HelpCommand( ) );
      return helpMenu;
    }


  }
}
