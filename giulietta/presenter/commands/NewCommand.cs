//
//  NewCommand.cs
//
//  Author:
//       carddamom <carddamom@outlook.pt>
//
//  Copyright (c) 2016 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using Eto.Drawing;
using System;
using cc.carddamom.giulietta.view.dialogs;
using Eto.Forms;

namespace cc.carddamom.giulietta.presenter {

  /// <summary>
  /// Command that creates a new workspace, project or file
  /// </summary>
  public class NewCommand : Command {

    private NewDialog dialog;

    /// <summary>
    /// Initializes a new instance of the <see cref="T:cc.carddamom.giulietta.presenter.NewCommand"/> class.
    /// </summary>
    public NewCommand( NewDialog dialog ) {
      MenuText = "New...";
      Shortcut = Application.Instance.CommonModifier | Keys.N;
      Image = Bitmap.FromResource( "folder_add.png" );
      ToolTip = "Creates a new workspace, project or file";
      this.dialog = dialog;
    }

    /// <summary>
    /// Runs the command
    /// </summary>
    protected override void OnExecuted( EventArgs e ) {
      dialog.ShowModal( );
    }
  }
}
