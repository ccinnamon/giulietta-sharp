//
//  DeleteCommand.cs
//
//  Author:
//       carddamom <carddamom@outlook.pt>
//
//  Copyright (c) 2016 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using Eto.Drawing;


namespace cc.carddamom.giulietta.presenter {
  using System;
  using Eto.Forms;


  public class DeleteCommand : Command {

    public DeleteCommand ( ) {

      MenuText = "Delete";
      Shortcut = Application.Instance.CommonModifier | Keys.Delete;
      Image = Bitmap.FromResource( "cross.png" );
      ToolTip = "Deletes the selected files or text";

    }

    protected override void OnExecuted ( EventArgs e ) {
    }
  }
}
