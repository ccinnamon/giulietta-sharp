//
//  OpenCommand.cs
//
//  Author:
//       carddamom <carddamom@outlook.pt>
//
//  Copyright (c) 2016 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using Eto.Drawing;

namespace cc.carddamom.giulietta.presenter {
  using System;
  using Eto.Forms;

  /// <summary>
  /// Open command.
  /// </summary>
  public class OpenCommand : Command {

    private OpenFileDialog dialog = new OpenFileDialog( );

    /// <summary>
    /// Initializes a new instance of the <see cref="T:cc.carddamom.giulietta.presenter.OpenCommand"/> class.
    /// </summary>
    public OpenCommand( OpenFileDialog dialog ) {
      this.dialog = dialog;
      MenuText = "Open...";
      Shortcut = Application.Instance.CommonModifier | Keys.O;
      Image = Bitmap.FromResource( "folder_page.png" );
      ToolTip = "Opens a workspace, project or file";
    }

    /// <summary>
    /// Runs the command
    /// </summary>
    protected override void OnExecuted( EventArgs e ) {
      dialog.ShowDialog( null );
    }
  }
}
