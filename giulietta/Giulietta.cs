﻿//
//  Giulietta.cs
//
//  Author:
//       carddamom <carddamom@outlook.pt>
//
//  Copyright (c) 2016 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Eto.Forms;
using cc.carddamom.giulietta.view;
using cc.carddamom.giulietta.view.dialogs;
using Autofac;
using Eto;

namespace giulietta {

  /// <summary>
  /// This is the main class for the giulietta editor.
  /// </summary>
  public abstract class Giulietta {

    private IContainer container;

    /// <summary>
    /// Initializes a new instance of the <see cref="T:giulietta.Giulietta"/> class.
    /// </summary>
    public Giulietta( ) {
      var builder = new ContainerBuilder( );
      builder.RegisterTypes( typeof( NewDialog ), typeof( OpenFileDialog ), typeof( SaveFileDialog ) );
      builder.RegisterType<MainForm>( );
      container = builder.Build( );
    }

    /// <summary>
    /// Gets the platform.
    /// </summary>
    protected abstract Platform getPlatform( );

    /// <summary>
    /// Run this instance.
    /// </summary>
    public void Run( ) {
      using( var scope = container.BeginLifetimeScope( ) ) {
        var app = new Application( getPlatform( ) );
        app.Run( scope.Resolve<MainForm>( ) );
      }
    }

    /// <summary>
    /// Generic giulietta.
    /// </summary>
    public class GenericGiulietta : Giulietta {

      /// <summary>
      /// Gets the platform.
      /// </summary>
      protected override Platform getPlatform( ) {
        return Platform.Detect;
      }
    }

    /// <summary>
    /// The entry point of the program, where the program control starts and ends.
    /// </summary>
    [STAThread]
    public static void Main( ) {
      var editor = new GenericGiulietta( );
      editor.Run( );
    }
  }
}
