In order to support multiple languages and tools, some written in languages different than go, the editor should support plugins. So there are two possible types of plugins:

- Extensions and Libraries &rarr; Where plugins are written using shared libraries, and where the main program accesses then after loading the library and can directly access the functions provided by the plugin;
- RPC &rarr; Where plugins are other programs that communicate with the main program using RPC (remote communication);

Having into account the advantages and disadvantages of the two, it was chosen the way of RPC, with the following most favorable transport methods:

- Named Pipes (Windows);
- Unix Sockets (*nix and OSX / MacOS);
- Regular Sockets (Using SCTP);

Regarding communication protocols, the D-node protocol is the chosen one.

In relation to the API in golang, it was considered the following:

## Server API - Inside the main application

- PluginRegistry &rarr; Log that contains a directory of all plugins, its location (how to connect to them) and the services they provide. This register is initialized at the beginning and destroyed at the end with a defer;
- PluginInfo &rarr; It contains all the basic information of a plugin that can be serialized;
- Plugin &rarr; Proxy that allows you to connect to a plugin and call the methods available for it;

Where PluginInfo contains:

- Id &rarr; Plugin Id (uuid);
- Name &rarr; Plugin name;
- Description &rarr; Description of the plugin;
- License &rarr; Plugin license using SPEX format;
- Author &rarr; Array with a list of authors (name and email);
- Version &rarr; Plugin semantic version (x.y.z);
- Interfaces &rarr; Array with a list of interfaces that the plugin implements;
- Path &rarr; Path to the plugin;
- Protocol &rarr; Transport Protocol used (named pipes, Unix sockets, SCTP);
- Port or File &rarr; Port name, pipe or unix socket which is used by the plugin;

In that Plugin contains:

- Connect(PluginInfo) &rarr; Establishes a connection to said plugin;
- Disconnect( ) &rarr; Closes the connection to the current plugin;
- Call(function, args ...) (ret, error) &rarr; Make a call to the respective function of the plugin, using the arguments data and returning the result or an error;
- Proxy(interface) (PluginProxy, error) &rarr; Returns an object that implements the given interface, in which calls to the interface methods correspond to the plugin calls;
- HasFunction(function, args..., retType) bool &rarr; Check if the current plugin implements the given method with the given type arguments and the data type of return;

Where PluginRegistry contains:

- Load( ) &rarr; Initialize the register;
- Unload( ) &rarr; Destroys the register;
- Register( PluginDirectory string ) &rarr; Go through the specified directory and try to detect the existing plugins;
- Unregister( PluginDirectory string ) &rarr; Scroll through the directory indicated in order to de-register all existing plugins in it;
- Register( PluginInfo ) &rarr; Register the plugin with the given information, checking as comprehensively as possible the veracity of the information;
- Unregister( PluginInfo ) &rarr; De-register the plugin with the given information;
- Search( SearchInfo ) &rarr; Search plugins that match the search data parameters;
- Load( PluginInfo ) ( Plugin, error ) &rarr; Loads the plugin and return a proxy for it;
- AddInterface( InterfaceName, PluginProxyFactory ) &rarr; Add the given interface with the given proxy factory so that it can be used in plugins that implement this interface;
- RemoveInterface( InterfaceName ) &rarr; Remove the given interface from the list of possible interfaces that can be implemented by plugins;

In that PluginProxy, implements the same interface as the plugin, but using remote calls to the methods.

Where SearchInfo contains:

- Name &rarr; expression that matches the name of the plugin that you want (can be a regular expression or part of the name);
- Interfaces &rarr; Interfaces that the plugin must implement;
- License &rarr; License that the plugin should have;
- Description &rarr; Expression that corresponds to the description of the plugin you want to find (can be a regular expression or part of the description);
- Version &rarr; Expression to filter a plugin depending on the version (same logic that NPM);

Where PluginProxyFactory contains:

- PluginProxyFactory( Plugin ) ( PluginProxy, error ) &rarr; Function that creates an interface proxy, given the plugin that is connected to it.

## Client Side - Inside the plugins

TODO