Since the user interface is the most important piece of code of Guilietta, the following is a mockup of the Guilietta user interface (This is done in ascii, even if the editor is in GTK+):

![Editor Mockup](https://raw.githubusercontent.com/wiki/carddamom/giulietta/images/giulietta.png)

As menus Guilietta has the following menus:

- AppMenu (OSX) &rarr; This is a menu that only exists on OSX and is typical of OSX native applications;
    - About Giulietta &rarr; Shows information about giulietta (like license, author names, etc.);
    - Preferences &rarr; Shows the editor preferences dialog;
    - Services &rarr; Shows services that can be used with the editor (OSX specific);
    - Hide Giulietta &rarr; Hides the editor window;
    - Hide Others &rarr; Hides all the other windows;
    - Show All &rarr; Shows the hidden windows;
    - Quit Giulietta &rarr; Quits the editor;
- File &rarr; This is a menu that has operations related to files, projects and workspaces;
    - New &rarr; Shows a dialog that allows the creation of a new workspace, project or file;
    - Open &rarr; Opens a workspace, project or file;
    - Open Recent &rarr; Opens a recent workspace, project or file (OSX Specific);
    - Close &rarr; Closes the currently open file;
    - Save &rarr; Saves the currently open file;
    - Save As... &rarr; Saves the currently open file with a new name;
    - Save All &rarr; Saves all open files;
    - Save All As... &rarr; Saves all open files with a new name;
    - Revert to Saved &rarr; Revert the changes of the currently open file;
    - Revert All to Saved &rarr; Reverts the changes of all open files;
    - Page Setup... &rarr; Shows the print configuration dialog;
    - Print &rarr; Prints using the last configured setups;
    - Quit (All except OSX) &rarr; Quits the editor
- Edit &rarr; Shows options related to file editing
    - Undo &rarr; Undoes the last change to the currently open file;
    - Redo &rarr; Redoes the last change to the currently open file;
    - Cut &rarr; Cuts the currently selected text;
    - Copy &rarr; Copies the currently selected text;
    - Paste &rarr; Pastes the clipboard text into the cursor;
    - Delete &rarr; Deletes the currently selected text;
    - Select All &rarr; Selects all lines from the current file;
    - Find &rarr; Shows options related to text search;
        - Find... &rarr; Shows a dialog to allow the user to search for text;
        - Find and Replace... &rarr; Shows a dialog to allow the user to search and replace text;
        - Find Next &rarr; Shows the next match, using the previous configurations;
        - Find Previous &rarr; Shows the previous match, using the previous configurations;
    - Preferences (All except OSX) &rarr; Shows the editor preferences dialog;
- Window &rarr; Shows options related to the editor window;
    - Minimize &rarr; Minimizes the editor window;
    - Zoom &rarr; ???
    - Bring All to Front &rarr; Brings all windows to front (including non-modal dialogs);
    - Window List... &rarr; Shows the list of all open windows (including dialogs);
- Help &rarr; Shows help information
    - Application Help &rarr; Shows the editor help
    - Version (All except OSX) &rarr; Shows information about giulietta (like license, author names, etc.);