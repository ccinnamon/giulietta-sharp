In order to support the creation of workspaces, projects, files and other types of stuff that is better handled by wizards, Giulietta includes an wizard related fluent-interface for the creation of then.

In a high level view, the wizards have the following lifecycle (at least the one's created by this interface):

![Wizard Workflow Diagram](https://raw.githubusercontent.com/wiki/carddamom/giulietta/images/diagrams.png)

So we have, in golang the following API:

- Wizard &rarr; Structure that maintains information about the wizards;
- Step &rarr; Structure that maintains information about steps of a wizard;
- WizardBuilder &rarr; Structure that creates the new wizard steps using a fluent interface;
- WizardBuild &rarr; Function that creates a new wizard with the given name, title and steps (WizardBuilder);
- WizardStep &rarr; Interface that defines a wizard step container that contains the ui elements for the wizard step (graphical widgets);
- WizardFrame &rarr; Contains the general frame of the wizard with the left image, the buttons for back, forward, cancel and finish;
- CommonWizardSteps &rarr; Contains step panels commonly used and easily configurable, like asking for a string, filename, etc.
- WizardStepsUtilities &rarr; Contains a series of functions to create panels, from GTK+ ui files, etc.

Where Wizard contains the following:

- name (string) &rarr; Represents the name of the wizard, for plugin identification purposes;
- title (string) &rarr; The title of the tutorial, that appears in its window;
- type (string) &rarr; The type of tutorial for sorting purposes (for example, project creation, refactor, etc.);
- steps (array of step) &rarr; List of steps that the wizard contains;
- end &rarr; Closure that is executed when the wizard terminates (ie. the user finishes it);
- TODO

TODO