This project is a text editor written in golang using GTK+.

This project has the desirable features:

- Support for projects, which are file groupings;
- Text editor with syntax highlighting, right margin and counting of line numbers;
- Text Editor with virtual desktop to be able to have the code centered on the screen, directly under the user eyes;
- Support for help tools with functions of a library or language, using the dash or other similar tool;
- Support for linting tools or code verification;
- Support for building and testing systems (can be in a later stage);
- Support for tasks (to be implemented at a later stage);
- Integration with version control systems (to be implemented at a later stage);

It has the aim of being a source code editor, that follows the Unix tradition of using multiple tools, specialized for the job, like linters, compilers, etc.

And in spite, of being an IDE like editor, it does it with the help of individual tools, that are invoked, instead of a monolithic block, that does everything.