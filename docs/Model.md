Giulietta has the following high-level model:

```mermaid
graph LR;
    workspace;
    project;
    folder;
    file;
    tag;
    workspace-- zero or more -->project;
    workspace-- zero or more -->tag;
    project-- zero or more -->tag;
    project-- zero or more -->folder;
    project-- zero or more -->file;
    folder-- zero or more -->tag;
    folder-- zero or more -->file;
    file-- zero or more -->tag;
```

Where:

* Workspace &rarr; A workspace is a set of projects, that shares a specific editor configuration, like a workspace in eclipse;
* Project &rarr; A project is a set of related folders and files, that may be built and share some configuration, like library paths, binary paths, formatting settings, etc.
* Folder &rarr; A folder is a set of files, and it may or may not be backed by a real folder in the disk;
* File &rarr; A file contains some data, like text lines, it also has a mime-type, a size, permission, etc.
* Tag &rarr; A tag is a pair of key/value that may be used to add information to a workspace, project, folder or file.

As a way to extend workspaces, projects, folders and files there are two mechanisms:

* Tags &rarr; A simple way to associate a string, with a specific key, to a workspace, project, folder and file;
* Model &rarr; This is a way to associate arbitrary struts, with a specific key, to a workspace, project, folder and file.

## Field descrition

### workspace

A workspace has the following core members:

* name &rarr; The name of the workspace;
* id &rarr; An generated UUID of the workspace, that is unique among the generated local id's;
* path &rarr; The physical path where the workspace is stored;
* projects &rarr; The projects contained in the workspace;

There also operations to get and set the name and path. Also the ID is read-only for the user, so there is only a get operation. As for the projects, there are operations to add, remove, get, verify existance and count the number of projects.

### Project

A project has the following core members:

* name &rarr; The name of the project;
* id &rarr; The generated if of the project;
* state &rarr; The state of the project, either active or inactive;
* permission &rarr; The permission of the project, either read-only or writtable;
* homepage &rarr; The homepage of the project;
* maintainer &rarr; The name of the maintainer of the project in the format (name <email>);
* programmingLanguage &rarr; The programming language of the project or text for an unknown programming language;
* license &rarr; The license of the project;
* description &rarr; An description of the project;
* created &rarr; The time/date the project was created;
* bugTracker &rarr; Information about the project bug tracker;
* testers &rarr; A list of project testers in the format (name <email>);
* screenshots &rarr; A list of project screenshots (url's);
* translators &rarr; A list of project translators in the format (name <email>);
* documenters &rarr; A list of project documentation writers in the format (name <email>);
* platforms &rarr; A list of platforms where the project runs;
* mailingLists &rarr; A list of urls to the project mailing lists;
* developers &rarr; A list of project developers in the format (name <email>);
* repositories &rarr; A list of repositories with a copy of the project;

* folders &rarr; A list of folders contained by the project;

* files &rarr; A list of files contained by the root of the project;

There are also operations to:

* Read-write the name, state, permission, homepage, maintainer, programming language, license, description and bug tracker;
* Read the id and created date;
* Add, remove, get, search and count the testers, screenshots, translators, documenters, platforms, mailing lists, developers, repositories, folders and files;

### Folder

A folder has the following core members:

* name &rarr; The name of the folder;
* ftype &rarr; The type of the folder (virtual or physical);
* id &rarr; The generated id of the folder
* files &rarr; The files contained by the folder;

Also the name and type of the folder are read-write, the id is read-only and the files can be add, removed, counted, listed and searched.

### File

A file has the following core members:

* name &rarr; The name of the file;
* ftype &rarr; The type of the file (mime type); 
* file &rarr; A link to an file object (in the golang language);
* id &rarr; The generated if of the file;
* hash &rarr; An SHA-512 hash of the file;
* size &rarr; The size of the file;
* modificationStatus &rarr; The state of the file, for the editor, it can be PRISTINE (for newly created files), MODIFIED (for saved and edited files) and EDITING (for files being edited);
* filename &rarr; The name of the file on disk;

Also, there are operations to read and write the name, modificationStatus, filename; and to read the ftype, file, id, hash, size.

## Extensions

An model can be extended by using tags, that have the following signature:

* key &rarr; The key of the tag (always an string);
* value &rarr; The value of the tag (also a string);

A tag can be added, removed, retrieved, searched and their number retrieved for a workspace, project, file and folder.

An model can also be extended using arbitrary structures, with the following signature:




