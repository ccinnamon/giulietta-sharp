This file contains the structure of the giulietta project, on the repository.

Since giulietta is essentially a program that is going to use many external plugins
and those external plugins are going to use a API, the structure is as fllows:

* cmd &rarr; This contains the code specific to each executable binary, in the case of the editor frame, the code to create the menus, interface and process the events from the menus and interface. Each entry in here is a separate binary;
* config &rarr; This contains configuration related to the program, like the files specifying syntax;
* resources &rarr; Images or text strings, used by the program;
* shared &rarr; This contains a library with all the shared code, essentially the domain code, the plugin system and the wizard system.

Besides this the repository, follows the git flow architecture, where there is a branch for releases, production, development, hotfixes and features. Besides each version is named according to the semantic version rules and with a codename.

For the first three releases the codenames are:

* Slenderman;
* The rake;
* Whisper;