//
//  MailingList.cs
//
//  Author:
//       carddamom <carddamom@outlook.com>
//
//  Copyright (c) 2016 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace giulietta.core.domain {
  using System.Collections.Generic;
  using NanoByte.Common.Collections;
  using PropertyChanged;
  using PropertyChanging;


  /// This represents a project mailing list, and contains the address's to subscribe, unsubscribe,
  /// etc.
  [ImplementPropertyChanged]
  [ImplementPropertyChanging]
  [ToString]
  public class MailingListType {

    List<string> aliasesField;

    /// An human readable name for the mailing list
    public string Name { get; set; }

    /// The subscribe email.
    public string Subscribe { get; set; }

    /// The unsubscribe email
    public string Unsubscribe { get; set; }

    /// An email for posting to the mailing list.
    public string Post { get; set; }

    /// An url for viewing mailing list archives
    public string Archive { get; set; }


    public MailingListType() {
      this.aliasesField = new List<string>();
    }

    /// The other url's for viewing the archives of the maling list
    public IList<string> Aliases {
      get {
        return this.aliasesField.AsReadOnly();
      }
    }

    public void addAlias(string alias) {
      aliasesField.AddIfNew(alias);
    }

    public void removeAlias(string alias) {
      aliasesField.Remove(alias);
    }

    public bool containsAlias(string alias) {
      return aliasesField.Contains(alias);
    }

    public int countAlias() {
      return aliasesField.Count;
    }

  }
}
