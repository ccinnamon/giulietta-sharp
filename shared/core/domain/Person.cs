//
//  Person.cs
//
//  Author:
//       carddamom <carddamom@outlook.com>
//
//  Copyright (c) 2016 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace giulietta.core.domain {
  using PropertyChanged;
  using PropertyChanging;

  /// This represens an Person (eg. an Developer, documenter, etc.)
  [ImplementPropertyChanged]
  [ImplementPropertyChanging]
  [ToString]
  public class PersonType {

    /// The name of the person.
    public string Name { get; set; }

    /// The email of the person
    public string Email { get; set; }

  }
}
