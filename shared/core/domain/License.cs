//
//  License.cs
//
//  Author:
//       carddamom <carddamom@outlook.com>
//
//  Copyright (c) 2016 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using PropertyChanged;
using PropertyChanging;

namespace giulietta.core.domain {

  /// This represents a project license and contains the url to it, name, etc.
  [ImplementPropertyChanged]
  [ImplementPropertyChanging]
  [ToString]
  public class LicenseType {

    /// An human readable name for the license
    public string Name { get; set; }

    /// The url that contains the text of the license (URL)
    public Uri Url { get; set; }

    /// An SPDX License tag
    public SPDXLicensesEnum Spdx { get; set; }

    /// What the license covers (eg. Source code, Images, etc.)
    public string Coverage { get; set; }

  }
}
