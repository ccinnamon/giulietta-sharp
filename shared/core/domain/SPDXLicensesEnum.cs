//
//  SPDXLicensesEnum.cs
//
//  Author:
//       carddamom <carddamom@outlook.com>
//
//  Copyright (c) 2016 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace giulietta.core.domain {

  public enum SPDXLicensesEnum {

    Glide,

    Abstyles,

    [SPDXLicenseValue("AFL-1.1")]
    AFL11,

    [SPDXLicenseValue("AFL-1.2")]
    AFL12,

    [SPDXLicenseValue("AFL-2.0")]
    AFL20,

    [SPDXLicenseValue("AFL-2.1")]
    AFL21,

    [SPDXLicenseValue("AFL-3.0")]
    AFL30,

    AMPAS,

    [SPDXLicenseValue("APL-1.0")]
    APL10,

    [SPDXLicenseValue("Adobe-Glyph")]
    AdobeGlyph,

    APAFML,

    [SPDXLicenseValue("Adobe-2006")]
    Adobe2006,

    [SPDXLicenseValue("AGPL-1.0")]
    AGPL10,

    Afmparse,

    Aladdin,

    ADSL,

    AMDPLPA,

    [SPDXLicenseValue("ANTLR-PD")]
    ANTLRPD,

    [SPDXLicenseValue("Apache-1.0")]
    Apache10,

    [SPDXLicenseValue("Apache-1.1")]
    Apache11,

    [SPDXLicenseValue("Apache-2.0")]
    Apache20,

    AML,

    [SPDXLicenseValue("APSL-1.0")]
    APSL10,

    [SPDXLicenseValue("APSL-1.1")]
    APSL11,

    [SPDXLicenseValue("APSL-1.2")]
    APSL12,

    [SPDXLicenseValue("APSL-2.0")]
    APSL20,

    [SPDXLicenseValue("Artistic-1.0")]
    Artistic10,

    [SPDXLicenseValue("Artistic-1.0-Perl")]
    Artistic10Perl,

    [SPDXLicenseValue("Artistic-1.0-cl8")]
    Artistic10cl8,

    [SPDXLicenseValue("Artistic-2.0")]
    Artistic20,

    AAL,

    Bahyph,

    Barr,

    Beerware,

    [SPDXLicenseValue("BitTorrent-1.0")]
    BitTorrent10,

    [SPDXLicenseValue("BitTorrent-1.1")]
    BitTorrent11,

    [SPDXLicenseValue("BSL-1.0")]
    BSL10,

    Borceux,

    [SPDXLicenseValue("BSD-2-Clause")]
    BSD2Clause,

    [SPDXLicenseValue("BSD-2-Clause-FreeBSD")]
    BSD2ClauseFreeBSD,

    [SPDXLicenseValue("BSD-2-Clause-NetBSD")]
    BSD2ClauseNetBSD,

    [SPDXLicenseValue("BSD-3-Clause")]
    BSD3Clause,

    [SPDXLicenseValue("BSD-3-Clause-Clear")]
    BSD3ClauseClear,

    [SPDXLicenseValue("BSD-3-Clause-No-Nuclear-License")]
    BSD3ClauseNoNuclearLicense,

    [SPDXLicenseValue("BSD-3-Clause-No-Nuclear-License-2014")]
    BSD3ClauseNoNuclearLicense2014,

    [SPDXLicenseValue("BSD-3-Clause-No-Nuclear-Warranty")]
    BSD3ClauseNoNuclearWarranty,

    [SPDXLicenseValue("BSD-4-Clause")]
    BSD4Clause,

    [SPDXLicenseValue("BSD-Protection")]
    BSDProtection,

    [SPDXLicenseValue("BSD-Source-Code")]
    BSDSourceCode,

    [SPDXLicenseValue("BSD-3-Clause-Attribution")]
    BSD3ClauseAttribution,

    [SPDXLicenseValue("0BSD")]
    Item0BSD,

    [SPDXLicenseValue("BSD-4-Clause-UC")]
    BSD4ClauseUC,

    [SPDXLicenseValue("bzip2-1.0.5")]
    bzip2105,

    [SPDXLicenseValue("bzip2-1.0.6")]
    bzip2106,

    Caldera,

    [SPDXLicenseValue("CECILL-1.0")]
    CECILL10,

    [SPDXLicenseValue("CECILL-1.1")]
    CECILL11,

    [SPDXLicenseValue("CECILL-2.0")]
    CECILL20,

    [SPDXLicenseValue("CECILL-2.1")]
    CECILL21,

    [SPDXLicenseValue("CECILL-B")]
    CECILLB,

    [SPDXLicenseValue("CECILL-C")]
    CECILLC,

    ClArtistic,

    [SPDXLicenseValue("MIT-CMU")]
    MITCMU,

    [SPDXLicenseValue("CNRI-Jython")]
    CNRIJython,

    [SPDXLicenseValue("CNRI-Python")]
    CNRIPython,

    [SPDXLicenseValue("CNRI-Python-GPL-Compatible")]
    CNRIPythonGPLCompatible,

    [SPDXLicenseValue("CPOL-1.02")]
    CPOL102,

    [SPDXLicenseValue("CDDL-1.0")]
    CDDL10,

    [SPDXLicenseValue("CDDL-1.1")]
    CDDL11,

    [SPDXLicenseValue("CPAL-1.0")]
    CPAL10,

    [SPDXLicenseValue("CPL-1.0")]
    CPL10,

    [SPDXLicenseValue("CATOSL-1.1")]
    CATOSL11,

    [SPDXLicenseValue("Condor-1.1")]
    Condor11,

    [SPDXLicenseValue("CC-BY-1.0")]
    CCBY10,

    [SPDXLicenseValue("CC-BY-2.0")]
    CCBY20,

    [SPDXLicenseValue("CC-BY-2.5")]
    CCBY25,

    [SPDXLicenseValue("CC-BY-3.0")]
    CCBY30,

    [SPDXLicenseValue("CC-BY-4.0")]
    CCBY40,

    [SPDXLicenseValue("CC-BY-ND-1.0")]
    CCBYND10,

    [SPDXLicenseValue("CC-BY-ND-2.0")]
    CCBYND20,

    [SPDXLicenseValue("CC-BY-ND-2.5")]
    CCBYND25,

    [SPDXLicenseValue("CC-BY-ND-3.0")]
    CCBYND30,

    [SPDXLicenseValue("CC-BY-ND-4.0")]
    CCBYND40,

    [SPDXLicenseValue("CC-BY-NC-1.0")]
    CCBYNC10,

    [SPDXLicenseValue("CC-BY-NC-2.0")]
    CCBYNC20,

    [SPDXLicenseValue("CC-BY-NC-2.5")]
    CCBYNC25,

    [SPDXLicenseValue("CC-BY-NC-3.0")]
    CCBYNC30,

    [SPDXLicenseValue("CC-BY-NC-4.0")]
    CCBYNC40,

    [SPDXLicenseValue("CC-BY-NC-ND-1.0")]
    CCBYNCND10,

    [SPDXLicenseValue("CC-BY-NC-ND-2.0")]
    CCBYNCND20,

    [SPDXLicenseValue("CC-BY-NC-ND-2.5")]
    CCBYNCND25,

    [SPDXLicenseValue("CC-BY-NC-ND-3.0")]
    CCBYNCND30,

    [SPDXLicenseValue("CC-BY-NC-ND-4.0")]
    CCBYNCND40,

    [SPDXLicenseValue("CC-BY-NC-SA-1.0")]
    CCBYNCSA10,

    [SPDXLicenseValue("CC-BY-NC-SA-2.0")]
    CCBYNCSA20,

    [SPDXLicenseValue("CC-BY-NC-SA-2.5")]
    CCBYNCSA25,

    [SPDXLicenseValue("CC-BY-NC-SA-3.0")]
    CCBYNCSA30,

    [SPDXLicenseValue("CC-BY-NC-SA-4.0")]
    CCBYNCSA40,

    [SPDXLicenseValue("CC-BY-SA-1.0")]
    CCBYSA10,

    [SPDXLicenseValue("CC-BY-SA-2.0")]
    CCBYSA20,

    [SPDXLicenseValue("CC-BY-SA-2.5")]
    CCBYSA25,

    [SPDXLicenseValue("CC-BY-SA-3.0")]
    CCBYSA30,

    [SPDXLicenseValue("CC-BY-SA-4.0")]
    CCBYSA40,

    [SPDXLicenseValue("CC0-1.0")]
    CC010,

    Crossword,

    CrystalStacker,

    [SPDXLicenseValue("CUA-OPL-1.0")]
    CUAOPL10,

    Cube,

    curl,

    [SPDXLicenseValue("D-FSL-1.0")]
    DFSL10,

    diffmark,

    WTFPL,

    DOC,

    Dotseqn,

    DSDP,

    dvipdfm,

    [SPDXLicenseValue("EPL-1.0")]
    EPL10,

    [SPDXLicenseValue("ECL-1.0")]
    ECL10,

    [SPDXLicenseValue("ECL-2.0")]
    ECL20,

    eGenix,

    [SPDXLicenseValue("EFL-1.0")]
    EFL10,

    [SPDXLicenseValue("EFL-2.0")]
    EFL20,

    [SPDXLicenseValue("MIT-advertising")]
    MITadvertising,

    [SPDXLicenseValue("MIT-enna")]
    MITenna,

    Entessa,

    [SPDXLicenseValue("ErlPL-1.1")]
    ErlPL11,

    EUDatagrid,

    [SPDXLicenseValue("EUPL-1.0")]
    EUPL10,

    [SPDXLicenseValue("EUPL-1.1")]
    EUPL11,

    Eurosym,

    Fair,

    [SPDXLicenseValue("MIT-feh")]
    MITfeh,

    [SPDXLicenseValue("Frameworx-1.0")]
    Frameworx10,

    FreeImage,

    FTL,

    FSFAP,

    FSFUL,

    FSFULLR,

    Giftware,

    GL2PS,

    Glulxe,

    [SPDXLicenseValue("AGPL-3.0")]
    AGPL30,

    [SPDXLicenseValue("GFDL-1.1")]
    GFDL11,

    [SPDXLicenseValue("GFDL-1.2")]
    GFDL12,

    [SPDXLicenseValue("GFDL-1.3")]
    GFDL13,

    [SPDXLicenseValue("GPL-1.0")]
    GPL10,

    [SPDXLicenseValue("GPL-2.0")]
    GPL20,

    [SPDXLicenseValue("GPL-3.0")]
    GPL30,

    [SPDXLicenseValue("LGPL-2.1")]
    LGPL21,

    [SPDXLicenseValue("LGPL-3.0")]
    LGPL30,

    [SPDXLicenseValue("LGPL-2.0")]
    LGPL20,

    gnuplot,

    [SPDXLicenseValue("gSOAP-1.3b")]
    gSOAP13b,

    HaskellReport,

    HPND,

    [SPDXLicenseValue("IBM-pibs")]
    IBMpibs,

    [SPDXLicenseValue("IPL-1.0")]
    IPL10,

    ICU,

    ImageMagick,

    iMatix,

    Imlib2,

    IJG,

    [SPDXLicenseValue("Info-ZIP")]
    InfoZIP,

    [SPDXLicenseValue("Intel-ACPI")]
    IntelACPI,

    Intel,

    [SPDXLicenseValue("Interbase-1.0")]
    Interbase10,

    IPA,

    ISC,

    [SPDXLicenseValue("JasPer-2.0")]
    JasPer20,

    JSON,

    [SPDXLicenseValue("LPPL-1.0")]
    LPPL10,

    [SPDXLicenseValue("LPPL-1.1")]
    LPPL11,

    [SPDXLicenseValue("LPPL-1.2")]
    LPPL12,

    [SPDXLicenseValue("LPPL-1.3a")]
    LPPL13a,

    [SPDXLicenseValue("LPPL-1.3c")]
    LPPL13c,

    Latex2e,

    [SPDXLicenseValue("BSD-3-Clause-LBNL")]
    BSD3ClauseLBNL,

    Leptonica,

    LGPLLR,

    Libpng,

    libtiff,

    [SPDXLicenseValue("LAL-1.2")]
    LAL12,

    [SPDXLicenseValue("LAL-1.3")]
    LAL13,

    [SPDXLicenseValue("LiLiQ-P-1.1")]
    LiLiQP11,

    [SPDXLicenseValue("LiLiQ-Rplus-1.1")]
    LiLiQRplus11,

    [SPDXLicenseValue("LiLiQ-R-1.1")]
    LiLiQR11,

    [SPDXLicenseValue("LPL-1.02")]
    LPL102,

    [SPDXLicenseValue("LPL-1.0")]
    LPL10,

    MakeIndex,

    MTLL,

    [SPDXLicenseValue("MS-PL")]
    MSPL,

    [SPDXLicenseValue("MS-RL")]
    MSRL,

    MirOS,

    MITNFA,

    MIT,

    Motosoto,

    [SPDXLicenseValue("MPL-1.0")]
    MPL10,

    [SPDXLicenseValue("MPL-1.1")]
    MPL11,

    [SPDXLicenseValue("MPL-2.0")]
    MPL20,

    [SPDXLicenseValue("MPL-2.0-no-copyleft-exception")]
    MPL20nocopyleftexception,

    mpich2,

    Multics,

    Mup,

    [SPDXLicenseValue("NASA-1.3")]
    NASA13,

    Naumen,

    [SPDXLicenseValue("NBPL-1.0")]
    NBPL10,

    NetCDF,

    NGPL,

    NOSL,

    [SPDXLicenseValue("NPL-1.0")]
    NPL10,

    [SPDXLicenseValue("NPL-1.1")]
    NPL11,

    Newsletr,

    NLPL,

    Nokia,

    [SPDXLicenseValue("NPOSL-3.0")]
    NPOSL30,

    [SPDXLicenseValue("NLOD-1.0")]
    NLOD10,

    Noweb,

    NRL,

    NTP,

    Nunit,

    [SPDXLicenseValue("OCLC-2.0")]
    OCLC20,

    [SPDXLicenseValue("ODbL-1.0")]
    ODbL10,

    [SPDXLicenseValue("PDDL-1.0")]
    PDDL10,

    [SPDXLicenseValue("OCCT-PL")]
    OCCTPL,

    OGTSL,

    [SPDXLicenseValue("OLDAP-2.2.2")]
    OLDAP222,

    [SPDXLicenseValue("OLDAP-1.1")]
    OLDAP11,

    [SPDXLicenseValue("OLDAP-1.2")]
    OLDAP12,

    [SPDXLicenseValue("OLDAP-1.3")]
    OLDAP13,

    [SPDXLicenseValue("OLDAP-1.4")]
    OLDAP14,

    [SPDXLicenseValue("OLDAP-2.0")]
    OLDAP20,

    [SPDXLicenseValue("OLDAP-2.0.1")]
    OLDAP201,

    [SPDXLicenseValue("OLDAP-2.1")]
    OLDAP21,

    [SPDXLicenseValue("OLDAP-2.2")]
    OLDAP22,

    [SPDXLicenseValue("OLDAP-2.2.1")]
    OLDAP221,

    [SPDXLicenseValue("OLDAP-2.3")]
    OLDAP23,

    [SPDXLicenseValue("OLDAP-2.4")]
    OLDAP24,

    [SPDXLicenseValue("OLDAP-2.5")]
    OLDAP25,

    [SPDXLicenseValue("OLDAP-2.6")]
    OLDAP26,

    [SPDXLicenseValue("OLDAP-2.7")]
    OLDAP27,

    [SPDXLicenseValue("OLDAP-2.8")]
    OLDAP28,

    OML,

    [SPDXLicenseValue("OPL-1.0")]
    OPL10,

    [SPDXLicenseValue("OSL-1.0")]
    OSL10,

    [SPDXLicenseValue("OSL-1.1")]
    OSL11,

    [SPDXLicenseValue("OSL-2.0")]
    OSL20,

    [SPDXLicenseValue("OSL-2.1")]
    OSL21,

    [SPDXLicenseValue("OSL-3.0")]
    OSL30,

    OpenSSL,

    [SPDXLicenseValue("OSET-PL-2.1")]
    OSETPL21,

    [SPDXLicenseValue("PHP-3.0")]
    PHP30,

    [SPDXLicenseValue("PHP-3.01")]
    PHP301,

    Plexus,

    PostgreSQL,

    psfrag,

    psutils,

    [SPDXLicenseValue("Python-2.0")]
    Python20,

    [SPDXLicenseValue("QPL-1.0")]
    QPL10,

    Qhull,

    Rdisc,

    [SPDXLicenseValue("RPSL-1.0")]
    RPSL10,

    [SPDXLicenseValue("RPL-1.1")]
    RPL11,

    [SPDXLicenseValue("RPL-1.5")]
    RPL15,

    [SPDXLicenseValue("RHeCos-1.1")]
    RHeCos11,

    RSCPL,

    [SPDXLicenseValue("RSA-MD")]
    RSAMD,

    Ruby,

    [SPDXLicenseValue("SAX-PD")]
    SAXPD,

    Saxpath,

    SCEA,

    SWL,

    SMPPL,

    Sendmail,

    [SPDXLicenseValue("SGI-B-1.0")]
    SGIB10,

    [SPDXLicenseValue("SGI-B-1.1")]
    SGIB11,

    [SPDXLicenseValue("SGI-B-2.0")]
    SGIB20,

    [SPDXLicenseValue("OFL-1.0")]
    OFL10,

    [SPDXLicenseValue("OFL-1.1")]
    OFL11,

    [SPDXLicenseValue("SimPL-2.0")]
    SimPL20,

    Sleepycat,

    SNIA,

    [SPDXLicenseValue("Spencer-86")]
    Spencer86,

    [SPDXLicenseValue("Spencer-94")]
    Spencer94,

    [SPDXLicenseValue("Spencer-99")]
    Spencer99,

    SMLNJ,

    [SPDXLicenseValue("SugarCRM-1.1.3")]
    SugarCRM113,

    SISSL,

    [SPDXLicenseValue("SISSL-1.2")]
    SISSL12,

    [SPDXLicenseValue("SPL-1.0")]
    SPL10,

    [SPDXLicenseValue("Watcom-1.0")]
    Watcom10,

    TCL,

    Unlicense,

    TMate,

    [SPDXLicenseValue("TORQUE-1.1")]
    TORQUE11,

    TOSL,

    [SPDXLicenseValue("Unicode-TOU")]
    UnicodeTOU,

    [SPDXLicenseValue("UPL-1.0")]
    UPL10,

    NCSA,

    Vim,

    VOSTROM,

    [SPDXLicenseValue("VSL-1.0")]
    VSL10,

    [SPDXLicenseValue("W3C-19980720")]
    W3C19980720,

    W3C,

    Wsuipa,

    Xnet,

    X11,

    Xerox,

    [SPDXLicenseValue("XFree86-1.1")]
    XFree8611,

    xinetd,

    xpp,

    XSkat,

    [SPDXLicenseValue("YPL-1.0")]
    YPL10,

    [SPDXLicenseValue("YPL-1.1")]
    YPL11,

    Zed,

    [SPDXLicenseValue("Zend-2.0")]
    Zend20,

    [SPDXLicenseValue("Zimbra-1.3")]
    Zimbra13,

    [SPDXLicenseValue("Zimbra-1.4")]
    Zimbra14,

    Zlib,

    [SPDXLicenseValue("zlib-acknowledgement")]
    zlibacknowledgement,

    [SPDXLicenseValue("ZPL-1.1")]
    ZPL11,

    [SPDXLicenseValue("ZPL-2.0")]
    ZPL20,

    [SPDXLicenseValue("ZPL-2.1")]
    ZPL21
  }
}