//
//  BugTracker.cs
//
//  Author:
//       carddamom <carddamom@outlook.com>
//
//  Copyright (c) 2016 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using PropertyChanged;
using PropertyChanging;

namespace giulietta.core.domain {

  ///<summary>This represents an bug tracker of an project.</summary>
  [ImplementPropertyChanged]
  [ImplementPropertyChanging]
  [ToString]
  public class BugTrackerType {

    ///<summary>An human readable name for the bug tracker (eg. Github Bug Tracker).</summary>
    public string Name { get; set; }

    ///<summary>The url for accessing the bug tracker.</summary>
    public Uri Url { get; set; }

    ///<summary>The url for an API access to the Bug Tracker (optional).</summary>
    public Uri ApiUrl { get; set; }

    ///<summary>An human readable description of the bug tracker, eg. How to use it, What to report, etc.</summary>
    public string Description { get; set; }

  }
}
