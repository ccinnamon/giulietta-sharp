//
//  Repository.cs
//
//  Author:
//       carddamom <carddamom@outlook.com>
//
//  Copyright (c) 2016 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;

namespace giulietta.core.domain {
  using PropertyChanged;
  using PropertyChanging;

  /// This represents an project repository.
  [ImplementPropertyChanged]
  [ImplementPropertyChanging]
  [ToString]
  public class RepositoryType {

    /// The url to use to clone or obtain the sources from the repository.
    public Uri Connection { get; set; }

    /// The url for an developer with proper authorization to clone and commit changes to the
    /// repository.
    public Uri DeveloperConnection { get; set; }

    /// The default branch of the repository.
    public string Tag { get; set; }

    /// The url for viewing the repository contents.
    public Uri Url { get; set; }

    /// The type of the repository (eg. git, hg (mercurial), svn (subversion), etc.)
    public string Type { get; set; }

  }

}
