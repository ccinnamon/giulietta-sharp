//
//  Workspace.cs
//
//  Author:
//       carddamom <carddamom@outlook.com>
//
//  Copyright (c) 2016 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace giulietta.core.domain {

  using System;
  using System.Collections.Generic;
  using NanoByte.Common.Collections;
  using PropertyChanged;
  using PropertyChanging;

  /// <summary>
  /// Describes an workspace of the editor
  /// </summary>
  [ImplementPropertyChanged]
  [ImplementPropertyChanging]
  [ToString]
  public class WorkspaceType {

    List<ProjectType> projectField;

    /// The name of the workspace has shown by the editor
    public string Name { get; set; }

    /// The UUID of the workspace
    public Guid Id { get; private set; }

    /// The path of the workspace on disk
    public string Path { get; set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="T:giulietta.core.domain.WorkspaceType"/> class.
    /// </summary>
    public WorkspaceType() {
      this.projectField = new List<ProjectType>();
      this.Id = Guid.NewGuid();
    }

    /// An list of projects contained in the workspace
    public IList<ProjectType> project {
      get {
        return this.projectField.AsReadOnly();
      }
    }

    public void addProject(ProjectType project) {
      projectField.AddIfNew(project);
    }

    public void removeProject(ProjectType project) {
      projectField.Remove(project);
    }

    public bool containsProject(ProjectType project) {
      return projectField.Contains(project);
    }

    public int countProject() {
      return projectField.Count;
    }
  }
}
