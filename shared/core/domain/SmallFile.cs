//
//  SmallFile.cs
//
//  Author:
//       carddamom <carddamom@outlook.com>
//
//  Copyright (c) 2016 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace giulietta.core.domain {
  using PropertyChanged;
  using PropertyChanging;

  /// This represents an file like an image and contains the name, path and optionaly its content.
  [ImplementPropertyChanged]
  [ImplementPropertyChanging]
  [ToString]
  public class SmallFileType {

    byte[] binaryDataField;

    /// The name of the small file
    public string Name { get; set; }

    /// The path of the small file on disk or it's url
    public string Path { get; set; }

    /// An description of the file (eg. The image legend)
    public string Description { get; set; }

    /// The base64 binary data of the file (optional) (base64)
    public byte[] BinaryData {
      get {
        return this.binaryDataField;
      }
      set {
        this.binaryDataField = value;
      }
    }
  }

}
