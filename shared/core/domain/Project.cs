//
//  Project.cs
//
//  Author:
//       carddamom <carddamom@outlook.com>
//
//  Copyright (c) 2016 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using NanoByte.Common.Collections;
using PropertyChanged;
using PropertyChanging;

namespace giulietta.core.domain {

  ///This represents an project in the view of the editor.
  [ImplementPropertyChanged]
  [ImplementPropertyChanging]
  [ToString]
  public class ProjectType {

    List<string> programmingLanguageField;

    List<string> platformField;

    List<LicenseType> licenseField;

    List<PersonType> testerField;

    List<SmallFileType> screenshotField;

    List<PersonType> translatorField;

    List<PersonType> documenterField;

    List<MailingListType> mailingListField;

    List<PersonType> developerField;

    List<RepositoryType> repositoryField;

    List<FileType> fileField;

    List<FolderType> folderField;

    /// An human readable name for the project that is shown in the editor
    public string Name { get; set; }

    /// The UUID of the project
    public Guid Id { get; private set; }

    /// The state of the project
    public ProjectStateEnum State { get; set; }

    /// What the user can do with the project
    public ProjectPermissionEnum Permission { get; set; }

    /// The homepage of the project
    public Uri Homepage { get; set; }

    public PersonType Maintainer { get; set; }

    /// An human readable description of the project
    public string Description { get; set; }

    /// The date the project was created in the editor
    public DateTime Created { get; private set; }

    /// The bug tracker of the project
    public BugTrackerType BugTracker { get; set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="T:giulietta.core.domain.ProjectType"/> class.
    /// </summary>
    public ProjectType() {
      this.folderField = new List<FolderType>();
      this.fileField = new List<FileType>();
      this.repositoryField = new List<RepositoryType>();
      this.developerField = new List<PersonType>();
      this.mailingListField = new List<MailingListType>();
      this.platformField = new List<string>();
      this.documenterField = new List<PersonType>();
      this.translatorField = new List<PersonType>();
      this.screenshotField = new List<SmallFileType>();
      this.testerField = new List<PersonType>();
      this.licenseField = new List<LicenseType>();
      this.programmingLanguageField = new List<string>();
      this.Id = Guid.NewGuid();
      this.Created = DateTime.Now;
    }

    /// The project programming languages
    public IList<string> ProgrammingLanguage {
      get {
        return this.programmingLanguageField.AsReadOnly();
      }
    }

    /// <summary>
    /// Adds a programming language.
    /// </summary>
    /// <param name="programmingLanguage">The programming language to add.</param>
    public void addProgrammingLanguage(string programmingLanguage) {
      programmingLanguageField.AddIfNew(programmingLanguage);
    }

    /// <summary>
    /// Removes a programming language.
    /// </summary>
    /// <param name="programmingLanguage">The programming language to remove</param>
    public void removeProgrammingLanguage(string programmingLanguage) {
      programmingLanguageField.Remove(programmingLanguage);
    }

    /// <summary>
    /// If the project contains the given programming language.
    /// </summary>
    /// <returns><c>true</c>, if programming language was contained, <c>false</c> otherwise.</returns>
    /// <param name="programmingLanguage">Programming language.</param>
    public bool containsProgrammingLanguage(string programmingLanguage) {
      return programmingLanguageField.Contains(programmingLanguage);
    }

    /// <summary>
    /// The number of programming languages of the project.
    /// </summary>
    /// <returns>The programming language.</returns>
    public int countProgrammingLanguage() {
      return programmingLanguageField.Count;
    }

    /// The licenses used in the project
    public IList<LicenseType> License {
      get {
        return this.licenseField.AsReadOnly();
      }
    }

    /// <summary>
    /// Adds a new license.
    /// </summary>
    /// <param name="license">The license to add</param>
    public void addLicense(LicenseType license) {
      licenseField.AddIfNew(license);
    }

    public void removeLicense(LicenseType license) {
      licenseField.Remove(license);
    }

    public bool containsLicense(LicenseType license) {
      return licenseField.Contains(license);
    }

    public int countLicense() {
      return licenseField.Count;
    }

    /// The name of the testers of the project
    public IList<PersonType> Tester {
      get {
        return this.testerField.AsReadOnly();
      }
    }

    public void addTester(PersonType tester) {
      testerField.AddIfNew(tester);
    }

    public void removeTester(PersonType tester) {
      testerField.Remove(tester);
    }

    public bool containsTester(PersonType tester) {
      return testerField.Contains(tester);
    }

    public int countTester() {
      return testerField.Count;
    }

    /// An list of screenshots of the project
    public IList<SmallFileType> Screenshot {
      get {
        return this.screenshotField.AsReadOnly();
      }
    }

    public void addScreenshot(SmallFileType screenshot) {
      screenshotField.AddIfNew(screenshot);
    }

    public void removeScreenshot(SmallFileType screenshot) {
      screenshotField.Remove(screenshot);
    }

    public bool containsScreenshot(SmallFileType screenshot) {
      return screenshotField.Contains(screenshot);
    }

    public int countScreenshot() {
      return screenshotField.Count;
    }

    /// The name of the translators of the project
    public IList<PersonType> Translator {
      get {
        return this.translatorField.AsReadOnly();
      }
    }

    public void addTranslator(PersonType translator) {
      translatorField.AddIfNew(translator);
    }

    public void removeTranslator(PersonType translator) {
      translatorField.Remove(translator);
    }

    public bool containsTranslator(PersonType translator) {
      return translatorField.Contains(translator);
    }

    public int countTranslator() {
      return translatorField.Count;
    }

    /// The name of the documenters of the project
    public IList<PersonType> Documenter {
      get {
        return this.documenterField.AsReadOnly();
      }
    }

    public void addDocumenter(PersonType documenter) {
      documenterField.AddIfNew(documenter);
    }

    public void removeDocumenter(PersonType documenter) {
      documenterField.Remove(documenter);
    }

    public bool containsDocumenter(PersonType documenter) {
      return documenterField.Contains(documenter);
    }

    public int countDocumenter() {
      return documenterField.Count;
    }

    /// The platforms in which the project runs
    public IList<string> Platform {
      get {
        return this.platformField.AsReadOnly();
      }
    }

    public void addPlatform(string platform) {
      platformField.AddIfNew(platform);
    }

    public void removePlatform(string platform) {
      platformField.Remove(platform);
    }

    public bool containsPlatform(string platform) {
      return platformField.Contains(platform);
    }

    public int countPlatform() {
      return platformField.Count;
    }

    /// The mailing lists for the project
    public IList<MailingListType> MailingList {
      get {
        return this.mailingListField.AsReadOnly();
      }
    }

    public void addMailingList(MailingListType mailingList) {
      mailingListField.AddIfNew(mailingList);
    }

    public void removeMailingList(MailingListType mailingList) {
      mailingListField.Remove(mailingList);
    }

    public bool containsMailingList(MailingListType mailingList) {
      return mailingListField.Contains(mailingList);
    }

    public int countMailingList() {
      return mailingListField.Count;
    }

    /// The developers of the project
    public IList<PersonType> Developer {
      get {
        return this.developerField.AsReadOnly();
      }
    }

    public void addDeveloper(PersonType developer) {
      developerField.AddIfNew(developer);
    }

    public void removeDeveloper(PersonType developer) {
      developerField.Remove(developer);
    }

    public bool containsDeveloper(PersonType developer) {
      return developerField.Contains(developer);
    }

    public int countDeveloper() {
      return developerField.Count;
    }

    /// The source code repositories of the project
    public IList<RepositoryType> Repository {
      get {
        return this.repositoryField.AsReadOnly();
      }
    }

    public void addRepository(RepositoryType repository) {
      repositoryField.AddIfNew(repository);
    }

    public void removeRepository(RepositoryType repository) {
      repositoryField.Remove(repository);
    }

    public bool containsRepository(RepositoryType repository) {
      return repositoryField.Contains(repository);
    }

    public int countRepository() {
      return repositoryField.Count;
    }

    /// The files contained at the root of the project
    public IList<FileType> File {
      get {
        return this.fileField.AsReadOnly();
      }
    }

    public void addFile(FileType file) {
      fileField.AddIfNew(file);
    }

    public void removeFile(FileType file) {
      fileField.Remove(file);
    }

    public bool containsFile(FileType file) {
      return fileField.Contains(file);
    }

    public int countFile() {
      return fileField.Count;
    }

    /// The folders contained by the project
    public IList<FolderType> Folder {
      get {
        return this.folderField.AsReadOnly();
      }
    }

    public void addFolder(FolderType folder) {
      folderField.AddIfNew(folder);
    }

    public void removeFolder(FolderType folder) {
      folderField.Remove(folder);
    }

    public bool containsFolder(FolderType folder) {
      return folderField.Contains(folder);
    }

    public int countFolder() {
      return folderField.Count;
    }
  }
}
