//
//  File.cs
//
//  Author:
//       carddamom <carddamom@outlook.com>
//
//  Copyright (c) 2016 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using PropertyChanged;
using PropertyChanging;

namespace giulietta.core.domain {

  ///<summary> This represents an project file</summary>
  [ImplementPropertyChanged]
  [ImplementPropertyChanging]
  [ToString]
  public class FileType {

    string path;

    ///<summary> The human readable name for the file, as shown in the editor</summary>
    public string Name { get; set; }

    ///<summary> The mime type of the file as defined by RFC 2045 and IANA.</summary>
    public string ContentType { get; set; }

    ///<summary> The path of the file on disk</summary>
    public string Path {
      get {
        return path;
      }
      set {
        if(File.Exists( value ) ) {
          var fileP = new FileInfo(value);
          var hash = new SHA512Managed().ComputeHash(fileP.OpenRead());

          var sb = new StringBuilder();

          foreach (byte b in hash) {
            sb.AppendFormat("{0:x2}", b);
          }

          Hash = sb.ToString();

          Size = fileP.Length;
        }
        path = value;
      } 
    }

    ///<summary> The UUID of the file</summary>
    public Guid Id { get; private set; }

    ///<summary> An SHA-512 hash of the file</summary>
    public string Hash { get; private set; }

    ///<summary> The size of the file in bytes</summary>
    public long Size { get; private set; }

    ///<summary> The state of the file in the editor</summary>
    public ModificationStatusEnum ModificationStatus { get; set; }

    public FileType( ) {
      Id = Guid.NewGuid();
    }

  }
}
