//
//  Folder.cs
//
//  Author:
//       carddamom <carddamom@outlook.com>
//
//  Copyright (c) 2016 carddamom
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using NanoByte.Common.Collections;

namespace giulietta.core.domain {
  using System;
  using System.Collections.Generic;
  using PropertyChanged;
  using PropertyChanging;


  ///<summary> This defines an folder of the project</summary>
  [ImplementPropertyChanged]
  [ImplementPropertyChanging]
  [ToString]
  public class FolderType {

    List<FileType> fileField;

    readonly List<FolderType> folderField;

    ///<summary> The name of the folder as shown by the editor</summary>
    public string Name { get; set; }

    ///<summary> The type of folder (virtual or not)</summary>
    public FolderTypeEnum Type { get; set; }

    ///<summary> The UUID of the folder</summary>
    public Guid Id { get; private set; }


    public FolderType() {
      folderField = new List<FolderType>();
      fileField = new List<FileType>();
      Id = Guid.NewGuid();
    }

    ///<summary> The files contained by this folder</summary>
    public IList<FileType> Files {
      get {
        return fileField.AsReadOnly();
      }
    }

    public void addFiles(FileType file) {
      fileField.AddIfNew( file );
    }

    public void removeFiles(FileType file) {
      fileField.Remove(file);
    }

    public bool containsFiles(FileType file) {
      return fileField.Contains(file);
    }

    public int countFiles() {
      return fileField.Count;
    }

    ///<summary> The folders contained by this folder</summary>
    public IList<FolderType> Folders {
      get {
        return folderField.AsReadOnly();
      }
    }

    public void addFolders(FolderType folder) {
      folderField.AddIfNew(folder);
    }

    public void removeFolders(FolderType folder) {
      folderField.Remove(folder);
    }

    public bool containsFolders(FolderType folder) {
      return folderField.Contains(folder);
    }

    public int countFolders() {
      return folderField.Count;
    }

  }
}
