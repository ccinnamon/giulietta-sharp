﻿//
//  Copyright 2016  Copyright 2016 Carddamom
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using System;
using Microsoft.Build.Utilities;
using Microsoft.Build.Framework;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using HandlebarsDotNet;
using System.IO;

namespace html {

  public class AppendToTemplate : Task {

    /// <summary>
    /// Gets or sets the input containing the files to apply to the template
    /// </summary>
    [Required]
    public ITaskItem2[ ] Input { get; set; }

    /// <summary>
    /// Gets or sets the output directory.
    /// </summary>
    [Required]
    public string Output { get; set; }

    /// <summary>
    /// Gets or sets the template file to use, in mustache format.
    /// </summary>
    [Required]
    public string TemplateFile { get; set; }

    public override bool Execute ( ) {

      foreach( var taskItem in Input ) {

        var document = new HtmlDocument( );
        document.Load( taskItem.ItemSpec );

        var element = document.CreateElement( "ul" );

        CreateTableOfContents( document, element, document );

        Handlebars.RegisterHelper( "toc", ( output, context, arguments ) => {
          output.WriteLine( element.WriteTo( ) );
        } );

        Handlebars.RegisterHelper( "content", ( output, context, arguments ) => {
          output.WriteLine( document.DocumentNode.WriteTo( ) );
        } );

        var compile = Handlebars.Compile( new StreamReader( TemplateFile ) );

        var outputFile = Path.Combine( Output, Path.GetFileNameWithoutExtension( taskItem.ItemSpec ) + ".html" );

        if( File.Exists( outputFile ) ) {
          File.Delete( outputFile );
        }

        compile( new StreamWriter( outputFile ), new { } );

      }

      return true;
    }

    int getNodeLevel ( HtmlNode node ) {
      switch( node.Name.ToLower( ) ) {
      case "h1":
        return 1;
      case "h2":
        return 2;
      case "h3":
        return 3;
      case "h4":
        return 4;
      case "h5":
        return 5;
      case "h6":
        return 6;
      default:
        return 0;
      }
    }

    void CreateTableOfContents ( HtmlDocument finalDocument, HtmlNode ulElement, HtmlDocument doc ) {

      var xpath = "//*[self::h1 or self::h2 or self::h3 or self::h4 or self::h5 or self::h6]";
      if( doc.DocumentNode.SelectNodes( xpath ) != null ) {
        var level = 0;

        var list = new List<HtmlNode>( doc.DocumentNode.SelectNodes( xpath ) );
        list.Add( list.Last( ) );

        var sb = new StringBuilder( );

        string id;
        HtmlNode liElement, aElement, topLink;

        var listCount = list.Count - 1;

        for( int i = 0; i < listCount; i++ ) {

          var node = list[ i ];
          var next = list[ i + 1 ];

          var levelnode = getNodeLevel( node );
          var levelnext = getNodeLevel( next );

          id = "ID" + Guid.NewGuid( ).ToString( ).Replace( "-", "" );
          node.SetAttributeValue( "id", id );
          topLink = finalDocument.CreateElement( "a" );
          topLink.SetAttributeValue( "href", "#" );
          topLink.AppendChild( finalDocument.CreateTextNode( node.InnerText ) );
          node.RemoveAllChildren( );
          node.AppendChild( topLink );

          liElement = finalDocument.CreateElement( "li" );
          aElement = finalDocument.CreateElement( "a" );
          aElement.AppendChild( finalDocument.CreateTextNode( node.InnerText ) );
          aElement.SetAttributeValue( "href", "#" + id );
          liElement.AppendChild( aElement );
          sb.Append( liElement.WriteTo( ) );

          if( levelnode.CompareTo( levelnext ) < 0 ) { //i < i+1
            sb.Append( "<ul>" );
            level = level + 1;

          } else if( levelnode.CompareTo( levelnext ) > 0 ) { //i > i+1

            if( level > 0 ) {
              sb.Append( "</ul>" );
            }

            level = level - 1;
          }
        }

        for( int i = level - 1; i >= 0; i-- ) {

          sb.Append( "</ul>" );

        }

        var temp = new HtmlDocument( );
        temp.LoadHtml( sb.ToString( ) );

        ulElement.AppendChild( temp.DocumentNode );
      }
    }

  }
}
