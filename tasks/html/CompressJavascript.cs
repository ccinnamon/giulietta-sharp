﻿//
//  Copyright 2016  Copyright 2016 Carddamom
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using Yahoo.Yui.Compressor;

namespace html {
  public class CompressJavascript : Task {

    [Required]
    public ITaskItem2[ ] Input { get; set; }

    public ITaskItem2[ ] Output { get; set; }

    public override bool Execute ( ) {

      var InputOutputMap = new List<Tuple<string, StringBuilder, string>>( );

      if( Output == null ) {
        var InputLength = Input.Length;
        for( int i = 0; i < InputLength; i++ ) {
          InputOutputMap.Add( Tuple.Create( Input[ i ].ItemSpec, new StringBuilder( ), Input[ i ].ItemSpec ) );
        }
      } else {
        if( Input.Length <= Output.Length ) {
          var InputLength = Input.Length;
          for( int i = 0; i < InputLength; i++ ) {
            InputOutputMap.Add( Tuple.Create( Input[ i ].ItemSpec, new StringBuilder( ), Output[ i ].ItemSpec ) );
          }
        } else {
          if( Input.Length > Output.Length ) {

            var difference = Input.Length - Output.Length;

            var OutputLength = Output.Length;
            for( int i = 0; i < OutputLength; i++ ) {
              InputOutputMap.Add( Tuple.Create( Input[ i ].ItemSpec, new StringBuilder( ), Output[ i ].ItemSpec ) );
            }

            for( var i = 0; i < difference; i++ ) {
              InputOutputMap.Add( Tuple.Create( Input[ i + OutputLength ].ItemSpec, new StringBuilder( ), Input[ i + OutputLength ].ItemSpec ) );
            }
          }
        }
      }

      foreach( var entry in InputOutputMap ) {
        if( File.Exists( entry.Item1 ) ) {
          entry.Item2.Append( File.ReadAllText( entry.Item1 ) );

          var minifier = new JavaScriptCompressor( );
          minifier.CompressionType = CompressionType.Standard;
          minifier.DisableOptimizations = false;
          minifier.IgnoreEval = true;
          minifier.ObfuscateJavascript = true;

          using( var writer = new StreamWriter( entry.Item3, false, Encoding.UTF8 ) ) {
            writer.Write( minifier.Compress( entry.Item2.ToString( ) ) );
            writer.Flush( );
          }
        } else {
          Console.WriteLine( "The Javascript file " + entry.Item1 + " does not exist, skipping to next one..." );
        }
      }

      return true;
    }
  }
}
