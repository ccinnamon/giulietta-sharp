﻿//
//  Copyright 2016  
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.IO;
using Markdig;
using Ganss.XSS;
using System.Text;
using HtmlAgilityPack;

namespace tasks.markdown {

  /// <summary>
  /// Converts an markdown file to html, optionally sanitizing the resulting html
  /// </summary>
  public class MarkdownConverter : Task {

    /// <summary>
    /// The markdown files to convert.
    /// </summary>
    [Required]
    public ITaskItem[ ] Input { get; set; }

    /// <summary>
    /// The output file to write the resulting html into.
    /// </summary>
    [Required]
    public string OutputDirectory { get; set; }

    /// <summary>
    /// If the resulting html is gonna be sanitized.
    /// </summary>
    public bool Sanitize { get; set; } = true;

    /// <summary>
    /// The path to the stylesheets to add to the generated html file
    /// </summary>
    public ITaskItem[ ] Styles { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="T:tasks.markdown.MarkdownConverter"/> outputs 
    /// a full page or only a fragment.
    /// </summary>
    public bool IsFullPage { get; set; } = true;

    /// <summary>
    /// Converts the markdown input to html, sanitizing it (optionally) and writing the result into Output.
    /// </summary>
    public override bool Execute ( ) {
      if( !Directory.Exists( OutputDirectory ) ) {
        Directory.CreateDirectory( OutputDirectory );
      }
      foreach( ITaskItem input in Input ) {
        if( !convertFile( input.ItemSpec ) ) {
          Console.WriteLine( "Uanble to convert " + input.ItemSpec + " to markdown..." );
        }
      }
      return true;
    }

    HtmlNode createAndAppend ( HtmlDocument document, string name, HtmlNode node ) {
      var element = document.CreateElement( name );
      node.AppendChild( element );
      return element;
    }

    HtmlNode createAndAppend ( HtmlDocument document, string name, string text, HtmlNode node ) {
      var element = document.CreateElement( name );
      element.AppendChild( document.CreateTextNode( text ) );
      node.AppendChild( element );
      return element;
    }

    bool convertFile ( string input ) {
      if( File.Exists( input ) ) {
        var output = Path.Combine( OutputDirectory, Path.GetFileNameWithoutExtension( input ) + ".html" );

        using( var reader = new StreamReader( input ) )
        using( var writer = new StreamWriter( output, false, Encoding.UTF8 ) ) {
          var settings = new MarkdownPipelineBuilder( ).UseDefinitionLists( ).UseFigures( ).UseGridTables( )
            .UsePipeTables( ).UseEmphasisExtras( ).UseTaskLists( ).UseFigures( ).UseFooters( ).UseMathematics( )
            .UseEmojiAndSmiley( ).UseSmartyPants( ).UseDiagrams( ).UseBootstrap( ).Build( );
          var result = Markdown.ToHtml( reader.ReadToEnd( ), settings );
          if( Sanitize ) {
            var sanitizer = new HtmlSanitizer( );
            sanitizer.AllowedAttributes.Add( "class" );
            result = sanitizer.Sanitize( result );
          }

          if( IsFullPage ) {
            var markdownFragment = new HtmlDocument( );
            markdownFragment.LoadHtml( result );

            var finalDocument = new HtmlDocument( );

            var htmlElement = createAndAppend( finalDocument, "html", finalDocument.DocumentNode );

            var headElement = createAndAppend( finalDocument, "head", htmlElement );

            createAndAppend( finalDocument, "title", Path.GetFileNameWithoutExtension( input ), headElement );

            if( Styles != null && Styles.Length > 0 ) {
              foreach( ITaskItem style in Styles ) {
                var linkEl = createAndAppend( finalDocument, "link", headElement );
                linkEl.SetAttributeValue( "type", "text/css" );
                linkEl.SetAttributeValue( "rel", "stylesheet" );
                linkEl.SetAttributeValue( "href", style.ItemSpec );
              }
            }

            var linkElement = createAndAppend( finalDocument, "link", headElement );
            linkElement.SetAttributeValue( "type", "text/css" );
            linkElement.SetAttributeValue( "rel", "stylesheet" );
            linkElement.SetAttributeValue( "href", "https://cdn.rawgit.com/knsv/mermaid/6.0.0/dist/mermaid.css" );

            createAndAppend( finalDocument, "script", headElement )
              .SetAttributeValue( "src", "https://cdn.rawgit.com/knsv/mermaid/6.0.0/dist/mermaid.min.js" );

            var bodyElement = createAndAppend( finalDocument, "body", htmlElement );

            var container = createAndAppend( finalDocument, "div", bodyElement );

            container.SetAttributeValue( "class", "container" );

            container.AppendChild( markdownFragment.DocumentNode );

            writer.Write( finalDocument.DocumentNode.WriteTo( ) );
          } else {
            writer.Write( result );
          }

          return true;
        }
      }

      return false;
    }
  }
}
