﻿//
//  Copyright 2016  carddamom
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Reflection;
using System.IO;
using NuDoq;
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Humanizer;

namespace documentation {

  public class DocumentationToMarkdown : Task {

    [Required]
    public string AssemblyFileName { get; set; }

    [Required]
    public string DocumentationFile { get; set; }

    public ITaskItem[ ] MembersToInclude { get; set; }

    [Required]
    public string OutputDirectory { get; set; }

    public override bool Execute ( ) {

      Directory.Delete( OutputDirectory, true );
      Directory.CreateDirectory( OutputDirectory );

      List<string> membersToInclude;
      try {
        membersToInclude = MembersToInclude.Select( ( ITaskItem arg ) => arg.ItemSpec ).ToList( );
      } catch( ArgumentNullException ) {
        membersToInclude = new List<string>( );
        membersToInclude.Add( "private" );
        membersToInclude.Add( "protected" );
        membersToInclude.Add( "public" );
      }

      if( File.Exists( AssemblyFileName ) && File.Exists( DocumentationFile ) ) {
        var doc = DocReader.Read( Assembly.LoadFrom( ( AssemblyFileName ) ), DocumentationFile );

        var memberVisitor = new MemberVisitor( );

        doc.Accept( memberVisitor );

        foreach( var member in memberVisitor.Stack ) {
          var path = Path.Combine( OutputDirectory, member.Item1.Transform( To.SentenceCase ) + ".md" );
          using( var writer = new StreamWriter( path ) ) {

            writer.WriteLine( "## Description" );
            writer.Write( member.Item2 );

            if( member.Item5.Length > 0 &&
               membersToInclude.Count( ( string arg ) => { return ( arg ?? "" ).ToLower( ).Equals( "private" ); } ) > 0 ) {

              writer.WriteLine( "## Private" );
              writer.Write( member.Item5 );
            }

            if( member.Item4.Length > 0 &&
               membersToInclude.Count( ( string arg ) => { return ( arg ?? "" ).ToLower( ).Equals( "protected" ); } ) > 0 ) {
              writer.WriteLine( "## Protected/Internal" );
              writer.Write( member.Item4 );
            }

            if( member.Item3.Length > 0 &&
               membersToInclude.Count( ( string arg ) => { return ( arg ?? "" ).ToLower( ).Equals( "public" ); } ) > 0 ) {
              writer.WriteLine( "## Public" );
              writer.Write( member.Item3 );
            }

            writer.Flush( );
          }
        }

      } else {

        var sb = new StringBuilder( );

        if( !File.Exists( AssemblyFileName ) ) {
          sb.Append( "The given assembly file ( " ).Append( AssemblyFileName ).Append( " ) does not exist\n" );
        }
        if( !File.Exists( DocumentationFile ) ) {
          sb.Append( "The given documentation file ( " ).Append( DocumentationFile ).Append( " ) does not exist" );
        }

        Console.Error.WriteLine( sb );
      }

      return true;
    }

  }

  class MemberVisitor : Visitor {

    readonly Stack<Tuple<string, StringBuilder, StringBuilder, StringBuilder, StringBuilder>> stack;

    /// <summary>
    /// Gets the current list of visited types, where the tuple members are:
    /// <list type="buttet">
    ///   <item><term>Name</term><description>An string with the name of the type</description></item>
    ///   <item><term>Description</term><description>An string with the description of the type</description></item>
    ///   <item><term>Public</term><description>An string with the public member descriptions</description></item>
    ///   <item><term>Protected</term><description>An string with the protected or internal member descriptions</description></item>
    ///   <item><term>Private</term><description>An string with the private member descriptions</description></item>
    /// </list>
    /// </summary>
    public Stack<Tuple<string, StringBuilder, StringBuilder, StringBuilder, StringBuilder>> Stack {
      get {
        return stack;
      }
    }

    public MemberVisitor ( ) {
      stack = new Stack<Tuple<string, StringBuilder, StringBuilder, StringBuilder, StringBuilder>>( );
    }

    public override void VisitInterface ( Interface type ) {
      var mark = new MarkdownVisitor( );
      type.Accept( mark );

      var sb = new StringBuilder( );
      sb.Append( "**Signature**: Interface " ).Append( TypeToString( type.Info as Type ) ).Append( "\n" );
      sb.Append( mark.ToString( ) ).Append( "\n" );

      var tuple = Tuple.Create( type.Info.Name.Replace( "`", "" ), sb, new StringBuilder( ), new StringBuilder( ), new StringBuilder( ) );
      Stack.Push( tuple );
      base.VisitInterface( type );
    }

    public override void VisitClass ( Class type ) {
      var mark = new MarkdownVisitor( );
      type.Accept( mark );

      var sb = new StringBuilder( );
      sb.Append( "**Signature**: Class " ).Append( TypeToString( type.Info as Type ) ).Append( "\n" );
      sb.Append( mark.ToString( ) ).Append( "\n" );

      var tuple = Tuple.Create( type.Info.Name.Replace( "`", "" ), sb, new StringBuilder( ), new StringBuilder( ), new StringBuilder( ) );
      Stack.Push( tuple );
      base.VisitClass( type );
    }

    public override void VisitStruct ( Struct type ) {
      var mark = new MarkdownVisitor( );
      type.Accept( mark );

      var sb = new StringBuilder( );
      sb.Append( "**Signature**: Struct " ).Append( TypeToString( type.Info as Type ) ).Append( "\n" );
      sb.Append( mark.ToString( ) ).Append( "\n" );

      var tuple = Tuple.Create( type.Info.Name.Replace( "`", "" ), sb, new StringBuilder( ), new StringBuilder( ), new StringBuilder( ) );
      Stack.Push( tuple );
      base.VisitStruct( type );
    }

    public override void VisitEnum ( NuDoq.Enum type ) {
      var mark = new MarkdownVisitor( );
      type.Accept( mark );

      var sb = new StringBuilder( );
      sb.Append( "**Signature**: Enum " ).Append( type.Info.Name ).Append( "\n" );
      sb.Append( mark.ToString( ) ).Append( "\n" );

      var tuple = Tuple.Create( type.Info.Name.Replace( "`", "" ), sb, new StringBuilder( ), new StringBuilder( ), new StringBuilder( ) );
      Stack.Push( tuple );
      base.VisitEnum( type );
    }

    public override void VisitField ( Field field ) {
      var mark = new MarkdownVisitor( );
      field.Accept( mark );

      if( stack.Count > 0 ) {
        var fi = field.Info as FieldInfo;

        if( fi.IsPrivate ) {
          Stack.Peek( ).Item5.Append( "\n### " ).Append( TypeToString( fi.FieldType ) )
               .Append( " " ).Append( fi.Name ).Append( "\n" ).Append( mark.ToString( ) ).Append( "\n" );
        } else {
          if( fi.IsPublic ) {
            Stack.Peek( ).Item3.Append( "\n### " ).Append( TypeToString( fi.FieldType ) )
               .Append( " " ).Append( fi.Name ).Append( "\n" ).Append( mark.ToString( ) ).Append( "\n" );
          } else {
            Stack.Peek( ).Item4.Append( "\n### " ).Append( TypeToString( fi.FieldType ) )
               .Append( " " ).Append( fi.Name ).Append( "\n" ).Append( mark.ToString( ) ).Append( "\n" );
          }
        }

      }

      base.VisitField( field );
    }

    string TypeToString ( Type type ) {
      if( type.IsGenericType ) {
        return GetCSharpRepresentation( type, true, type.GenericTypeArguments.ToList( ) );
      }

      return ( type.ReflectedType ?? type.DeclaringType ?? type ).Name;
    }


    //Code from http://stackoverflow.com/a/2579755
    //Thanks Adam Sills!
    #region ADAM_SILLS_CODE
    string GetCSharpRepresentation ( Type t, bool trimArgCount ) {
      if( t.IsGenericType ) {
        var genericArgs = t.GetGenericArguments( ).ToList( );
        return GetCSharpRepresentation( t, trimArgCount, genericArgs );
      }

      return ( t.ReflectedType ?? t.DeclaringType ?? t ).Name;
    }

    string GetCSharpRepresentation ( Type t, bool trimArgCount, List<Type> availableArguments ) {
      if( t.IsGenericType ) {
        string value = t.Name;
        if( trimArgCount && value.IndexOf( "`" ) > -1 ) {
          value = value.Substring( 0, value.IndexOf( "`" ) );
        }

        if( t.ReflectedType != null ) {
          // This is a nested type, build the nesting type first
          value = GetCSharpRepresentation( t.ReflectedType, trimArgCount, availableArguments ) + "+" + value;
        }

        // Build the type arguments (if any)
        string argString = "";
        var thisTypeArgs = t.GetGenericArguments( );
        for( int i = 0; i < thisTypeArgs.Length && availableArguments.Count > 0; i++ ) {
          if( i != 0 ) argString += ", ";

          argString += GetCSharpRepresentation( availableArguments[ 0 ], trimArgCount );
          availableArguments.RemoveAt( 0 );
        }

        // If there are type arguments, add them with < >
        if( argString.Length > 0 ) {
          value += "&lt;" + argString + "&gt;";
        }

        return value;
      }

      return t.Name;
    }
    #endregion

    public override void VisitProperty ( Property property ) {
      var mark = new MarkdownVisitor( );
      property.Accept( mark );

      if( stack.Count > 0 ) {
        var fi = property.Info as PropertyInfo;

        Stack.Peek( ).Item3.Append( "\n### " ).Append( TypeToString( fi.PropertyType ) ).Append( " " )
             .Append( fi.Name ).Append( "\n" ).Append( mark.ToString( ) ).Append( "\n" );
      }

      base.VisitProperty( property );
    }

    public override void VisitEvent ( Event @event ) {
      var mark = new MarkdownVisitor( );
      @event.Accept( mark );

      if( stack.Count > 0 ) {
        var fi = @event.Info as EventInfo;

        Stack.Peek( ).Item3.Append( "\n### " ).Append( TypeToString( fi.EventHandlerType ) ).Append( " " )
             .Append( fi.Name ).Append( "\n" ).Append( mark.ToString( ) ).Append( "\n" );
      }

      base.VisitEvent( @event );
    }

    public override void VisitMethod ( Method method ) {
      var mark = new MarkdownVisitor( );
      method.Accept( mark );

      if( stack.Count > 0 ) {
        var fi = ( MethodBase ) method.Info;

        if( fi.IsPrivate ) {
          Stack.Peek( ).Item5.Append( "\n### " );

          if( fi is MethodInfo ) {
            Stack.Peek( ).Item5.Append( ( fi as MethodInfo ).ReturnType != null ? TypeToString( ( fi as MethodInfo ).ReturnType ) : "" ).Append( " " );
          }

          Stack.Peek( ).Item5.Append( fi is MethodInfo ? fi.Name : "constructor" ).Append( "( " );

          var i = 0;

          foreach( var arguments in fi.GetParameters( ) ) {

            Stack.Peek( ).Item5.Append( TypeToString( arguments.ParameterType ) );

            if( fi.GetParameters( ).Length != ( i + 1 ) ) {
              Stack.Peek( ).Item5.Append( ", " );
            }
          }

          Stack.Peek( ).Item5.Append( " )\n" );
          Stack.Peek( ).Item5.Append( mark.ToString( ) ).Append( "\n" );
        } else {
          if( fi.IsPublic ) {
            Stack.Peek( ).Item3.Append( "\n### " );

            if( fi is MethodInfo ) {
              Stack.Peek( ).Item3.Append( ( fi as MethodInfo ).ReturnType != null ? TypeToString( ( fi as MethodInfo ).ReturnType ) : "" ).Append( " " );
            }

            Stack.Peek( ).Item3.Append( fi is MethodInfo ? fi.Name : "constructor" ).Append( "( " );

            var i = 0;

            foreach( var arguments in fi.GetParameters( ) ) {

              Stack.Peek( ).Item3.Append( TypeToString( arguments.ParameterType ) );

              if( fi.GetParameters( ).Length != ( i + 1 ) ) {
                Stack.Peek( ).Item3.Append( ", " );
              }
            }

            Stack.Peek( ).Item3.Append( " )\n" );
            Stack.Peek( ).Item3.Append( mark.ToString( ) ).Append( "\n" );
          } else {
            Stack.Peek( ).Item4.Append( "\n### " );

            if( fi is MethodInfo ) {
              Stack.Peek( ).Item4.Append( ( fi as MethodInfo ).ReturnType != null ? TypeToString( ( fi as MethodInfo ).ReturnType ) : "" ).Append( " " );
            }

            Stack.Peek( ).Item4.Append( fi is MethodInfo ? fi.Name : "constructor" ).Append( "( " );

            var i = 0;

            foreach( var arguments in fi.GetParameters( ) ) {

              Stack.Peek( ).Item4.Append( TypeToString( arguments.ParameterType ) );

              if( fi.GetParameters( ).Length != ( i + 1 ) ) {
                Stack.Peek( ).Item4.Append( ", " );
              }
            }

            Stack.Peek( ).Item4.Append( " )\n" );
            Stack.Peek( ).Item4.Append( mark.ToString( ) ).Append( "\n" );
          }
        }
      }

      base.VisitMethod( method );
    }

    public override void VisitExtensionMethod ( ExtensionMethod method ) {
      var mark = new MarkdownVisitor( );
      method.Accept( mark );

      if( stack.Count > 0 ) {
        var fi = method.Info as MethodInfo;

        if( fi.IsPrivate ) {
          Stack.Peek( ).Item5.Append( "\n### Ext " );
          Stack.Peek( ).Item5.Append( fi.ReturnType != null ? TypeToString( fi.ReturnType ) : "" ).Append( " " );
          Stack.Peek( ).Item5.Append( fi.Name ).Append( "( " );

          for( var j = 0; j < fi.GetParameters( ).Length; j++ ) {
            var arguments = fi.GetParameters( )[ j ];
            Stack.Peek( ).Item5.Append( TypeToString( arguments.ParameterType ) );
            if( fi.GetParameters( ).Length != ( j + 1 ) ) {
              Stack.Peek( ).Item5.Append( ", " );
            }
          }

          Stack.Peek( ).Item5.Append( " )\n" );
          Stack.Peek( ).Item5.Append( mark.ToString( ) ).Append( "\n" );
        } else {
          if( fi.IsPublic ) {
            Stack.Peek( ).Item3.Append( "\n### Ext " );
            Stack.Peek( ).Item3.Append( fi.ReturnType != null ? TypeToString( fi.ReturnType ) : "" ).Append( " " );
            Stack.Peek( ).Item3.Append( fi.Name ).Append( "( " );

            for( var j = 0; j < fi.GetParameters( ).Length; j++ ) {
              var arguments = fi.GetParameters( )[ j ];
              Stack.Peek( ).Item3.Append( TypeToString( arguments.ParameterType ) );
              if( fi.GetParameters( ).Length != ( j + 1 ) ) {
                Stack.Peek( ).Item3.Append( ", " );
              }
            }

            Stack.Peek( ).Item3.Append( " )\n" );
            Stack.Peek( ).Item3.Append( mark.ToString( ) ).Append( "\n" );
          } else {
            Stack.Peek( ).Item4.Append( "\n### Ext " );
            Stack.Peek( ).Item4.Append( fi.ReturnType != null ? TypeToString( fi.ReturnType ) : "" ).Append( " " );
            Stack.Peek( ).Item4.Append( fi.Name ).Append( "( " );

            for( var j = 0; j < fi.GetParameters( ).Length; j++ ) {
              var arguments = fi.GetParameters( )[ j ];
              Stack.Peek( ).Item4.Append( TypeToString( arguments.ParameterType ) );
              if( fi.GetParameters( ).Length != ( j + 1 ) ) {
                Stack.Peek( ).Item4.Append( ", " );
              }
            }

            Stack.Peek( ).Item4.Append( " )\n" );
            Stack.Peek( ).Item4.Append( mark.ToString( ) ).Append( "\n" );
          }
        }
      }

      base.VisitExtensionMethod( method );
    }

  }

  class MarkdownVisitor : Visitor {

    readonly StringBuilder mark = new StringBuilder( );

    public override void VisitSummary ( Summary summary ) {
      mark.Append( "**Summary**: " );
      base.VisitSummary( summary );
    }

    public override void VisitText ( Text text ) {
      mark.Append( text.Content );
      base.VisitText( text );
    }

    public override void VisitRemarks ( Remarks remarks ) {
      mark.Append( "**Remarks**: " );
      base.VisitRemarks( remarks );
    }

    public override void VisitPara ( Para para ) {
      base.VisitPara( para );
      mark.Append( "\n" );
    }

    public override void VisitCode ( Code code ) {
      mark.Append( "```" ).Append( code.Content ).Append( "```" );
      base.VisitCode( code );
    }

    public override void VisitC ( C code ) {
      mark.Append( "**" ).Append( code.Content ).Append( "**" );
      base.VisitC( code );
    }

    public override void VisitExample ( Example example ) {
      mark.Append( "**Example**: " );
      base.VisitExample( example );
    }

    public override void VisitSee ( See see ) {
      mark.Append( "See " ).Append( see.ToString( ) ).Append( " " ).Append( see.Content );
      base.VisitSee( see );
    }

    public override void VisitSeeAlso ( SeeAlso seeAlso ) {
      mark.Append( "See also: " ).Append( seeAlso.ToString( ) ).Append( " " ).Append( seeAlso.Content );
      base.VisitSeeAlso( seeAlso );
    }

    public override void VisitList ( List list ) {
      list.Accept( new ListVisitor( ) );
      list.Accept( new TableVisitor( ) );
      base.VisitList( list );
    }

    public override string ToString ( ) {
      return mark.ToString( );
    }

  }

  class TableVisitor : Visitor {

    readonly StringBuilder mark = new StringBuilder( );

    bool listHeader;

    public override void VisitListHeader ( ListHeader header ) {
      listHeader = true;
      mark.Append( "|" ).Append( header.Term ).Append( "|" ).Append( header.Description ).Append( "\n" );
      mark.Append( "|----------|-----------|" );
      base.VisitListHeader( header );
    }

    public override void VisitItem ( Item item ) {
      if( !listHeader ) {
        mark.Append( "|" ).Append( item.Term ).Append( "|" ).Append( item.Description ).Append( "\n" );
        base.VisitItem( item );
      }
    }

    public override string ToString ( ) {
      return mark.ToString( );
    }

  }

  class ListVisitor : Visitor {

    readonly StringBuilder mark = new StringBuilder( );

    public override void VisitItem ( Item item ) {
      mark.Append( item.Term ).Append( "\n" ).Append( ":" ).Append( item.Description );
      base.VisitItem( item );
    }

    public override string ToString ( ) {
      return mark.ToString( );
    }

  }

}
